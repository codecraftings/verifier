using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Verifier.IOS
{
	partial class JobDetailsViewController : UIViewController, IUITabBarDelegate
	{
		JobDescriptionView descriptionViewController;
		JobApplicantsListController applicantsViewController;
		public JobDetailsViewController (IntPtr handle) : base (handle)
		{
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			View.DefineLayout (new string[] {
				"|-0-[tabBar]-0-|"
			}, "tabBar", tabBar);
			tabBar.WeakDelegate = this;
			descriptionViewController = Storyboard.InstantiateViewController ("jobDescriptionView") as JobDescriptionView;
			applicantsViewController = Storyboard.InstantiateViewController ("jobApplicantsList") as JobApplicantsListController;
			this.AddChildViewController (descriptionViewController);
			this.AddChildViewController (applicantsViewController);
			tabBar.SelectedItem = descriptionButton;
			ItemSelected (tabBar, descriptionButton);
		}
		[Export ("tabBar:didSelectItem:")]
		public void ItemSelected (UIKit.UITabBar tabbar, UIKit.UITabBarItem item)
		{
			if (item.Tag == 1) {
				applicantsViewController.View.RemoveFromSuperview ();
				View.InsertSubviewBelow (descriptionViewController.View, tabBar);
			} else {
				descriptionViewController.View.RemoveFromSuperview ();
				View.InsertSubviewBelow (applicantsViewController.View, tabBar);
			}
		}
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			GlobalNavigationBar.HideMenuButton ();
			NavigationItem.SetHidesBackButton (false, true);
		}
	}
}
