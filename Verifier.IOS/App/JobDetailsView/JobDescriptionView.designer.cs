// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Verifier.IOS
{
	[Register ("JobDescriptionView")]
	partial class JobDescriptionView
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		NSLayoutConstraint descriptionCardTop { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView descriptionTF { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView scrollContainer { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (descriptionCardTop != null) {
				descriptionCardTop.Dispose ();
				descriptionCardTop = null;
			}
			if (descriptionTF != null) {
				descriptionTF.Dispose ();
				descriptionTF = null;
			}
			if (scrollContainer != null) {
				scrollContainer.Dispose ();
				scrollContainer = null;
			}
		}
	}
}
