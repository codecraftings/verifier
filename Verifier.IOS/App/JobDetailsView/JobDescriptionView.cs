using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Verifier.IOS
{
	partial class JobDescriptionView : UIViewController
	{
		public JobDescriptionView (IntPtr handle) : base (handle)
		{
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			scrollContainer.FixWeirdScrollViewError(View.Bounds.Width);
			descriptionCardTop.Constant = 25;
		}
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			descriptionTF.SizeToFit ();
		}
	}
}
