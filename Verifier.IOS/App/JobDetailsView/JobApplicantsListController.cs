using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Verifier.IOS
{
	partial class JobApplicantsListController : UIViewController, IUITableViewDataSource, IUITableViewDelegate
	{
		public JobApplicantsListController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			listTable.DataSource = this;
			listTable.Delegate = this;
			listTable.ContentInset = new UIEdgeInsets (50, 0, 0, 0);
			listTable.ScrollIndicatorInsets = new UIEdgeInsets (50, 0, 0, 0);
		}
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		[Export ("tableView:estimatedHeightForRowAtIndexPath:")]
		public System.nfloat EstimatedHeight (UIKit.UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			return 100;
		}
		public nint RowsInSection (UITableView tableView, nint section)
		{
			return 10;
		}
		public UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (ApplicantTableCell.KEY) as ApplicantTableCell;
			if (cell == null) {
				cell = new ApplicantTableCell ();
			}

			return cell;
		}
	}
}
