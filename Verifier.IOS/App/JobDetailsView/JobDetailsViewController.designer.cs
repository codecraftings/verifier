// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Verifier.IOS
{
	[Register ("JobDetailsViewController")]
	partial class JobDetailsViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITabBarItem applicantsButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITabBarItem descriptionButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITabBar tabBar { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (applicantsButton != null) {
				applicantsButton.Dispose ();
				applicantsButton = null;
			}
			if (descriptionButton != null) {
				descriptionButton.Dispose ();
				descriptionButton = null;
			}
			if (tabBar != null) {
				tabBar.Dispose ();
				tabBar = null;
			}
		}
	}
}
