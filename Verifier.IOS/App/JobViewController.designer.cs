// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Verifier.IOS
{
	[Register ("JobViewController")]
	partial class JobViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView applicantsHolder { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		NSLayoutConstraint applicantsListHeight { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView applicantsTable { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView jobActions { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel jobDate { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView jobDesc { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel jobLocation { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView jobThumb { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel jobTime { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel jobTitle { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView primaryInfoHolder { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView scrollContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView timeAndPlace { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (applicantsHolder != null) {
				applicantsHolder.Dispose ();
				applicantsHolder = null;
			}
			if (applicantsListHeight != null) {
				applicantsListHeight.Dispose ();
				applicantsListHeight = null;
			}
			if (applicantsTable != null) {
				applicantsTable.Dispose ();
				applicantsTable = null;
			}
			if (jobActions != null) {
				jobActions.Dispose ();
				jobActions = null;
			}
			if (jobDate != null) {
				jobDate.Dispose ();
				jobDate = null;
			}
			if (jobDesc != null) {
				jobDesc.Dispose ();
				jobDesc = null;
			}
			if (jobLocation != null) {
				jobLocation.Dispose ();
				jobLocation = null;
			}
			if (jobThumb != null) {
				jobThumb.Dispose ();
				jobThumb = null;
			}
			if (jobTime != null) {
				jobTime.Dispose ();
				jobTime = null;
			}
			if (jobTitle != null) {
				jobTitle.Dispose ();
				jobTitle = null;
			}
			if (primaryInfoHolder != null) {
				primaryInfoHolder.Dispose ();
				primaryInfoHolder = null;
			}
			if (scrollContainer != null) {
				scrollContainer.Dispose ();
				scrollContainer = null;
			}
			if (timeAndPlace != null) {
				timeAndPlace.Dispose ();
				timeAndPlace = null;
			}
		}
	}
}
