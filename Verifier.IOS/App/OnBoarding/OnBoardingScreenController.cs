using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using MediaPlayer;
using System.Threading.Tasks;
using Verifier.Core;
using Verifier.Shared;
namespace Verifier.IOS
{
	partial class OnBoardingScreenController : UIViewController, IViewController
	{
		public NavigationStore.Screen ScreenIdentifier {
			get {
				return NavigationStore.Screens.OnBoardScreen;
			}
		}

		MPMoviePlayerController movieBackground;
		SessionStore sessionStore;

		public bool IsViewVisible {
			get {
				return IsViewLoaded && View.Window!=null;
			}
		}

		public IIoCContainer IocContainer {
			get {
				return App.IocContainer;
			}
		}
		public IStoreManager storeManager;
		SessionActionsCreator sessionActionCreators;
		NavigationActionCreators navActionCreators;
		ReactBlock showEntryButtonsBlock;
		ReactBlock showWaitingBlock;
		ReactBlock showWelcomeBlock;
		public OnBoardingScreenController (IntPtr handle) : base (handle)
		{
			storeManager = IocContainer.Get<IStoreManager> ();
			sessionStore = storeManager.Bind<SessionStore> (this);
			sessionActionCreators = IocContainer.Get<SessionActionsCreator> ();
			navActionCreators = IocContainer.Get<NavigationActionCreators> ();
			showEntryButtonsBlock = ReactBlock.Register (ShowEntryButtons);
			showWaitingBlock = ReactBlock.Register (showWaitingSign);
			showWelcomeBlock = ReactBlock.Register (showWelcomeText);
		}


		public void RenderState ()
		{
			Console.WriteLine (sessionStore.State.Status);
			if (sessionStore.State.Status == SessionStore.StatusLoggedOut) {
				showEntryButtonsBlock.Invoke (sessionStore.State.Status);
			} else if (sessionStore.State.Status == SessionStore.StatusWaiting) {
				showWaitingBlock.Invoke (sessionStore.State.Status);
			} else if (sessionStore.State.Status == SessionStore.StatusLoggedIn) {
				showWelcomeBlock.Invoke (sessionStore.State.Status);
			}
		}
		void showWelcomeText(){
			loadingIndicator.StopAnimating ();
			loadingIndicator.Fade (AnimationTransition.Out);
			welcomeText.Hidden = false;
			welcomeText.Fade (AnimationTransition.In);
			buyerLoginButton.Fade (AnimationTransition.Out);
			verifierLoginButton.Fade (AnimationTransition.Out);
		}
		void showWaitingSign(){
			loadingIndicator.Hidden = false;
			loadingIndicator.Fade (AnimationTransition.In);
			loadingIndicator.StartAnimating ();
			welcomeText.Hidden = true;
			buyerLoginButton.Hidden = true;
			verifierLoginButton.Hidden = true;
		}
		void ShowEntryButtons(){
			loadingIndicator.StopAnimating ();
			loadingIndicator.Fade (AnimationTransition.Out);
			welcomeText.Hidden = true;
			buyerLoginButton.Hidden = false;
			verifierLoginButton.Hidden = false;
			buyerLoginButton.Fade (AnimationTransition.In);
			verifierLoginButton.Fade (AnimationTransition.In);
		}
		public async override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			NavigationController.NavigationBarHidden = true;
			movieBackground.Play ();
			RenderState ();
			while(!sessionStore.IsRehydrated)
				await Task.Delay(200);
			if (sessionStore.State.Status == SessionStore.StatusLoggedIn) {
				await Task.Delay (1000);
				sessionActionCreators.RestoreSession ();
			}
		}
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			movieBackground.Stop ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			NavigationController.NavigationBarHidden = true;
			initializeMoviePlayer ();
			animateLogoAppearance ();
			buyerLoginButton.TouchUpInside += (sender, e) => navActionCreators.NavigateToLogin(UserType.Buyer);
			verifierLoginButton.TouchUpInside += (sender, e) => navActionCreators.NavigateToLogin(UserType.Verifier);
			RenderState ();
		}

		void animateLogoAppearance ()
		{
			var initalTopMargin = logoTopMargin.Constant;
			logoTopMargin.Constant = UIScreen.MainScreen.Bounds.Height / 2 - logoView.Bounds.Height / 2;
			View.LayoutIfNeeded ();
			Task.Run (async () => {
				await Task.Delay(50);
				InvokeOnMainThread (() => {
					UIView.Animate (0.4f, 0f, UIViewAnimationOptions.CurveEaseInOut, () => {
						logoTopMargin.Constant = initalTopMargin;
						View.LayoutIfNeeded ();
					}, () => {
					});
				});
			});
		}

		void initializeMoviePlayer ()
		{
			movieBackground = new MPMoviePlayerController (NSBundle.MainBundle.GetUrlForResource ("video-background", "mp4"));
			movieBackground.ControlStyle = MPMovieControlStyle.None;
			movieBackground.ScalingMode = MPMovieScalingMode.AspectFill;
			movieBackground.InitialPlaybackTime = 0;
			movieBackground.EndPlaybackTime = 30;
			movieBackground.View.Frame = View.Frame;
			View.InsertSubviewBelow (movieBackground.View, overlay);
			movieBackground.Play ();
			NSNotificationCenter.DefaultCenter.AddObserver (MPMoviePlayerController.PlaybackDidFinishNotification, noti => {
				movieBackground.Play ();
			});
		}
	}
}
