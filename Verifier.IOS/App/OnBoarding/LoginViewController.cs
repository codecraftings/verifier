using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Threading.Tasks;
using Verifier.Core;
using Verifier.Shared;
namespace Verifier.IOS
{
	partial class LoginViewController : UIViewController, IViewController
	{
		public NavigationStore.Screen ScreenIdentifier {
			get {
				return NavigationStore.Screens.LoginScreen;
			}
		}
		public bool IsViewVisible {
			get {
				return IsViewLoaded && View.Window != null;
			}
		}

		public IIoCContainer IocContainer {
			get {
				return App.IocContainer;
			}
		}
		IStoreManager storeManager;
		NavigationActionCreators navActionCreator;
		SessionStore sessionStore;
		SessionActionsCreator sessionActionsCreator;
		ReactBlock toggleErrorBlock;
		ReactBlock toggleWaitingBlock;
		public LoginViewController (IntPtr handle) : base (handle)
		{
			storeManager = IocContainer.Get<IStoreManager> ();
			navActionCreator = IocContainer.Get<NavigationActionCreators> ();
			sessionStore = storeManager.Bind<SessionStore> (this);
			sessionActionsCreator = IocContainer.Get<SessionActionsCreator> ();
			toggleErrorBlock = ReactBlock.Register (this.toggleErrorNotice);
			toggleWaitingBlock = ReactBlock.Register (this.toggleWaitingIndicator);
		}
		public void RenderState ()
		{
			loginButton.Enabled = sessionStore.State.Status != SessionStore.StatusWaiting;
			toggleErrorBlock.Invoke (sessionStore.State.ErrorMessage);
			toggleWaitingBlock.Invoke (sessionStore.State.Status);
		}
		void toggleWaitingIndicator(){
			if (sessionStore.State.Status == SessionStore.StatusWaiting) {
				waitingIndicator.Fade (AnimationTransition.In);
				waitingIndicator.StartAnimating ();
			} else {
				waitingIndicator.Fade (AnimationTransition.Out);
				waitingIndicator.StopAnimating ();
			}
		}
		void toggleErrorNotice(){
			if (String.IsNullOrEmpty(sessionStore.State.ErrorMessage)) {
				errorNotice.Fade (AnimationTransition.Out, 0.3f, () => errorHeightConstraint.Constant = 0);
			} else {
				errorHeightConstraint.Constant = 30;
				errorNotice.Fade (AnimationTransition.In);
			}
		}
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			NavigationController.NavigationBarHidden = true;
			RenderState ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			errorHeightConstraint.Constant = 0;
			scrollContainer.FixWeirdScrollViewError (UIScreen.MainScreen.Bounds.Width);
			emailField.SetNextResponder (passwordField);
			emailField.SetKeyboardType (UIKeyboardType.EmailAddress);
			passwordField.MarkAsSecureField ();
			passwordField.SetDoneAction (() => HandleLoginSubmitted (this, null));
			new KeyboardVisibilityManager (scrollContainer, View).SetUp ();
			loginButton.TouchUpInside += HandleLoginSubmitted;

			backButton.TouchUpInside += (sender, e) => navActionCreator.Navigate(NavigationStore.Screens.OnBoardScreen);
		}

		void HandleLoginSubmitted (object sender, EventArgs e)
		{
			sessionActionsCreator.TryLogin (emailField.Value, passwordField.Value);	
		}
	}
}
