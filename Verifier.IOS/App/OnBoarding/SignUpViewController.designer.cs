// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Verifier.IOS
{
	[Register ("SignUpViewController")]
	partial class SignUpViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomInputView confirmPasswordField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomInputView emailField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel errorNotice { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		NSLayoutConstraint errorNoticeHeightConstraint { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIActivityIndicatorView loadingIndicator { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		SimpleSlickButton loginButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomInputView nameField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomInputView passwordField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		SimpleSlickButton registerButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView scrollContainer { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (confirmPasswordField != null) {
				confirmPasswordField.Dispose ();
				confirmPasswordField = null;
			}
			if (emailField != null) {
				emailField.Dispose ();
				emailField = null;
			}
			if (errorNotice != null) {
				errorNotice.Dispose ();
				errorNotice = null;
			}
			if (errorNoticeHeightConstraint != null) {
				errorNoticeHeightConstraint.Dispose ();
				errorNoticeHeightConstraint = null;
			}
			if (loadingIndicator != null) {
				loadingIndicator.Dispose ();
				loadingIndicator = null;
			}
			if (loginButton != null) {
				loginButton.Dispose ();
				loginButton = null;
			}
			if (nameField != null) {
				nameField.Dispose ();
				nameField = null;
			}
			if (passwordField != null) {
				passwordField.Dispose ();
				passwordField = null;
			}
			if (registerButton != null) {
				registerButton.Dispose ();
				registerButton = null;
			}
			if (scrollContainer != null) {
				scrollContainer.Dispose ();
				scrollContainer = null;
			}
		}
	}
}
