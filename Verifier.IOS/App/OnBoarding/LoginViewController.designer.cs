// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Verifier.IOS
{
	[Register ("LoginViewController")]
	partial class LoginViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton backButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomInputView emailField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		NSLayoutConstraint errorHeightConstraint { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel errorNotice { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		SimpleSlickButton forgetButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		SimpleSlickButton loginButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomInputView passwordField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		SimpleSlickButton registerButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView scrollContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIActivityIndicatorView waitingIndicator { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (backButton != null) {
				backButton.Dispose ();
				backButton = null;
			}
			if (emailField != null) {
				emailField.Dispose ();
				emailField = null;
			}
			if (errorHeightConstraint != null) {
				errorHeightConstraint.Dispose ();
				errorHeightConstraint = null;
			}
			if (errorNotice != null) {
				errorNotice.Dispose ();
				errorNotice = null;
			}
			if (forgetButton != null) {
				forgetButton.Dispose ();
				forgetButton = null;
			}
			if (loginButton != null) {
				loginButton.Dispose ();
				loginButton = null;
			}
			if (passwordField != null) {
				passwordField.Dispose ();
				passwordField = null;
			}
			if (registerButton != null) {
				registerButton.Dispose ();
				registerButton = null;
			}
			if (scrollContainer != null) {
				scrollContainer.Dispose ();
				scrollContainer = null;
			}
			if (waitingIndicator != null) {
				waitingIndicator.Dispose ();
				waitingIndicator = null;
			}
		}
	}
}
