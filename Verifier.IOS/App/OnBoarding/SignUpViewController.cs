using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using Verifier.Core;
using Verifier.Shared;

namespace Verifier.IOS
{
	partial class SignUpViewController : UIViewController, IViewController
	{
		IStoreManager storeManager;
		RegistrationStore registerStore;
		RegisterStoreActionCreators registerActionCreators;
		ReactBlock toggleLoadingBlock;
		ReactBlock toggleErrorNoticeBlock;
		public SignUpViewController (IntPtr handle) : base (handle)
		{
			storeManager = IocContainer.Get<IStoreManager> ();
			registerStore = storeManager.Bind<RegistrationStore> (this);
			registerActionCreators = IocContainer.Get<RegisterStoreActionCreators> ();
			toggleLoadingBlock = ReactBlock.Register (toggleLoadingIndicator);
			toggleErrorNoticeBlock = ReactBlock.Register (toggleErrorNotice);
		}

		public void RenderState ()
		{
			toggleErrorNoticeBlock.Invoke (registerStore.State.ErrorMessage);
			toggleLoadingBlock.Invoke (registerStore.State.Status);
		}
		void toggleErrorNotice(){
			if (!String.IsNullOrWhiteSpace(registerStore.State.ErrorMessage)) {
				errorNoticeHeightConstraint.Constant = 30;
				errorNotice.Text = registerStore.State.ErrorMessage;
				errorNotice.Fade (AnimationTransition.In);
			} else {
				errorNotice.Fade (AnimationTransition.Out, 0.3f, () => errorNoticeHeightConstraint.Constant = String.IsNullOrWhiteSpace(registerStore.State.ErrorMessage)? 0:30);
			}
		}
		void toggleLoadingIndicator(){
			if (registerStore.State.Status == RegistrationStore.StatusWaiting) {
				loadingIndicator.Fade (AnimationTransition.In);
				loadingIndicator.StartAnimating ();
			} else {
				loadingIndicator.Fade (AnimationTransition.Out);
				loadingIndicator.StopAnimating ();
			}
		}
		public NavigationStore.Screen ScreenIdentifier {
			get {
				return NavigationStore.Screens.RegisterScreen;
			}
		}


		public bool IsViewVisible {
			get {
				return IsViewLoaded && View.Window != null;
			}
		}


		public IIoCContainer IocContainer {
			get {
				return App.IocContainer;
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			new KeyboardVisibilityManager (scrollContainer, View).SetUp ();
			nameField.SetNextResponder (emailField);
			emailField.SetNextResponder (passwordField);
			passwordField.SetNextResponder (confirmPasswordField);
			passwordField.MarkAsSecureField ();
			confirmPasswordField.MarkAsSecureField ();
			loginButton.TouchUpInside += (object sender, EventArgs e) => NavigationController.PopViewController(true);
			registerButton.TouchUpInside += (sender, e) =>{
				registerActionCreators.TryRegister(
					nameField.Value,
					registerStore.State.IntendedUserType,
					emailField.Value,
					passwordField.Value,
					confirmPasswordField.Value
				);
			};
		}
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			NavigationController.NavigationBarHidden = true;
		}
	}

}
