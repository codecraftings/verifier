// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Verifier.IOS
{
	[Register ("OnBoardingScreenController")]
	partial class OnBoardingScreenController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		SimpleSlickButton buyerLoginButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIActivityIndicatorView loadingIndicator { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		NSLayoutConstraint logoTopMargin { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView logoView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView overlay { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		SimpleSlickButton verifierLoginButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel welcomeText { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (buyerLoginButton != null) {
				buyerLoginButton.Dispose ();
				buyerLoginButton = null;
			}
			if (loadingIndicator != null) {
				loadingIndicator.Dispose ();
				loadingIndicator = null;
			}
			if (logoTopMargin != null) {
				logoTopMargin.Dispose ();
				logoTopMargin = null;
			}
			if (logoView != null) {
				logoView.Dispose ();
				logoView = null;
			}
			if (overlay != null) {
				overlay.Dispose ();
				overlay = null;
			}
			if (verifierLoginButton != null) {
				verifierLoginButton.Dispose ();
				verifierLoginButton = null;
			}
			if (welcomeText != null) {
				welcomeText.Dispose ();
				welcomeText = null;
			}
		}
	}
}
