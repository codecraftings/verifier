using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using Verifier.Core;
using System.Collections.Generic;

namespace Verifier.IOS
{
	partial class PostedJobsController : UIViewController, IViewController, IUITableViewDataSource, IUITableViewDelegate
	{
		IStoreManager storeManager;
		JobsStore jobsStore;
		SessionStore sessionStore;
		JobsActionCreators jobsActionCreator;
		List<Job> jobsList;
		UIRefreshControl refreshControl;

		public PostedJobsController (IntPtr handle) : base (handle)
		{
			storeManager = IocContainer.Get<IStoreManager> ();
			jobsStore = storeManager.Bind<JobsStore> (this);
			sessionStore = storeManager.Bind<SessionStore> (this);
			jobsActionCreator = IocContainer.Get<JobsActionCreators> ();
			jobsList = new List<Job> ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ListTable.DataSource = this;
			ListTable.Delegate = this;
			ListTable.TableFooterView = new UIView (new CoreGraphics.CGRect(0, 0, ListTable.Bounds.Width, this.TabBarController.TabBar.Frame.Height));
			refreshControl = new UIRefreshControl ();
			refreshControl.ValueChanged += (object sender, EventArgs e) => jobsActionCreator.FetchPostedJobs (sessionStore.State.UserData, new JobFilter ());
//			var tableViewController = new UITableViewController ();
//			tableViewController.TableView = ListTable;
//			tableViewController.RefreshControl = refreshControl;
			ListTable.InsertSubview (refreshControl, 0);
			jobsActionCreator.FetchPostedJobs (sessionStore.State.UserData, new JobFilter ());
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			//refreshControl.BeginRefreshing ();
		}

		public void RenderState ()
		{
			if (jobsStore.State.Status == JobsStore.Status.FetchingList && !refreshControl.Refreshing) {
				refreshControl.BeginRefreshing ();
				ListTable.SetContentOffset (new CoreGraphics.CGPoint (0, ListTable.ContentOffset.Y - refreshControl.Frame.Height), true); 
				//ListTable.ContentInset = new UIEdgeInsets (0, 0, ListTable.RowHeight, 0);
			} else if (jobsStore.State.Status != JobsStore.Status.FetchingList && refreshControl.Refreshing)
				refreshControl.EndRefreshing ();
			jobsList = jobsStore.State.UserPostedJobsFiltered;
			ListTable.ReloadData ();
		}
		[Export ("tableView:didSelectRowAtIndexPath:")]
		public void RowSelected (UIKit.UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			var job = jobsList[indexPath.Row];
			jobsActionCreator.LoadJob (job);
		}
		public nint RowsInSection (UITableView tableView, nint section)
		{
			return jobsList.Count;
		}

		public UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var job = jobsList [indexPath.Row];
			var cell = (tableView.DequeueReusableCell ("postedJobList") as JobListCell) ?? new JobListCell ("postedJobList");
			cell.UpdateState (new JobCellState (){
				Title = job.Title,
				Address = job.Location.Address,
				CreateDate = job.CreatedAt,
				JobStatus = job.Status,
				ApplicantCount = job.Applicants.Count,
				ThumbImageUrl = job.Category.ThumbIcon
			});
			return cell;
		}

		public NavigationStore.Screen ScreenIdentifier {
			get {
				return new NavigationStore.Screen ();
			}
		}

		public bool IsViewVisible {
			get {
				return IsViewLoaded && View.Window != null;
			}
		}

		public IIoCContainer IocContainer {
			get {
				return Verifier.Shared.App.IocContainer;
			}
		}
	}
}
