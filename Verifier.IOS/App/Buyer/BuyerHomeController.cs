using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using Verifier.Core;

namespace Verifier.IOS
{
	partial class BuyerHomeController : UITabBarController,IViewController
	{
		public BuyerHomeController (IntPtr handle) : base (handle)
		{
		}

		public void RenderState ()
		{
			throw new NotImplementedException ();
		}

		public NavigationStore.Screen ScreenIdentifier {
			get {
				return NavigationStore.Screens.BuyerHome;
			}
		}

		public bool IsViewVisible {
			get {
				return IsViewLoaded && View.Window != null;
			}
		}

		public IIoCContainer IocContainer {
			get {
				return Verifier.Shared.App.IocContainer;
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			TabBar.TintColor = Styles.ThemeColor;
		}
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			NavigationController.SetNavigationBarHidden (false, true);
			NavigationItem.SetHidesBackButton (true, false);
			GlobalNavigationBar.ShowMenuButton ();
		}
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			GlobalNavigationBar.HideMenuButton ();
		}
	}
}
