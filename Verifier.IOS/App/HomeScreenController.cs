using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;

namespace Verifier.IOS
{
	partial class HomeScreenController : UIViewController, IUITableViewDataSource, IUITableViewDelegate
	{
		public HomeScreenController (IntPtr handle) : base (handle)
		{
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ListTable.DataSource = this;
			ListTable.ContentInset = new UIEdgeInsets (10, 0, 0, 0);
			ListTable.Delegate = this;
		}
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			NavigationController.SetNavigationBarHidden (false, true);
			NavigationItem.SetHidesBackButton (true, false);
			GlobalNavigationBar.ShowMenuButton ();
		}
		[Export ("tableView:estimatedHeightForRowAtIndexPath:")]
		public System.nfloat EstimatedHeight (UIKit.UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			return 100;
		}
		[Export ("tableView:didSelectRowAtIndexPath:")]
		public void RowSelected (UIKit.UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			var view = Storyboard.InstantiateViewController ("jobDetailsView");
			NavigationController.PushViewController (view, true);
			tableView.ReloadRows (new NSIndexPath[]{ indexPath }, UITableViewRowAnimation.Automatic);
		}
		public override CoreGraphics.CGSize PreferredContentSize {
			get {
				var size = base.PreferredContentSize;
				size.Height = 200;
				return size;
			}
			set {
				base.PreferredContentSize = value;
			}
		}
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			GlobalNavigationBar.HideMenuButton ();
			//NavigationItem.SetHidesBackButton (false, false);
		}

		public nint RowsInSection (UITableView tableView, nint section)
		{
			return DummyData.Jobs.Length;
		}

		public UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			return null;
//			var cell = tableView.DequeueReusableCell (ListTableCell.KEY) as ListTableCell;
//			if (cell == null) {
//				cell = new ListTableCell ();
//			}
//			var job = DummyData.Jobs [indexPath.Row];
//			cell.JobTitle = job.Item1;
//			cell.DatePosted = job.Item2;
//			cell.ThumbImage = UIImage.FromBundle (job.Item3);
//			cell.ApplicantsCount = job.Item4;
//			return cell;
		}
	}
}
