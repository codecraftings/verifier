using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using Verifier.Core;
using System.Collections.Generic;
using Splat;
namespace Verifier.IOS
{
	partial class JobViewController : UIViewController,IUITableViewDataSource, IUITableViewDelegate, IViewController
	{
		IStoreManager storeManager;
		JobsStore jobsStore;
		SessionStore sessionStore;
		List<JobApplicant> applicantsList;
		public JobViewController (IntPtr handle) : base (handle)
		{
			storeManager = IocContainer.Get<IStoreManager> ();
			jobsStore = storeManager.Bind<JobsStore> (this);
			sessionStore = storeManager.Bind<SessionStore> (this);
			applicantsList = new List<JobApplicant> ();
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			scrollContainer.FixWeirdScrollViewError (View.Bounds.Width);
			applicantsTable.DataSource = this;
			applicantsTable.FixTableViewEmptyCellError ();
		}
		Job lastLoadedJob; 
		public void RenderState ()
		{
			if (jobsStore.State.LoadedJob != null) {
				var newJob = jobsStore.State.LoadedJob;
				jobTitle.Text = newJob.Title.Substring(0, 20);
				jobDesc.Text = newJob.Description.Substring(0, 200);
				if (lastLoadedJob == null || lastLoadedJob.Category.ThumbIcon != newJob.Category.ThumbIcon) {
					setThumbImage (newJob.Category.ThumbIcon);
				}
				jobLocation.Text = newJob.Location.Address;
				jobDate.Text = newJob.DateTime.ToLongDateString ();
				jobTime.Text = "From 9am to 4pm";
				applicantsList = newJob.Applicants;
				applicantsListHeight.Constant = applicantsList.Count * ApplicantTableCell.EstHeight + 50;
				applicantsTable.ReloadData ();
				lastLoadedJob = newJob;
			}
		}
		async void setThumbImage(string url){
			var img = await IocContainer.Get<ICacheManager> ().GetImage (url);
			jobThumb.Image = img.ToNative ();
		}
		public NavigationStore.Screen ScreenIdentifier {
			get {
				return NavigationStore.Screens.JobViewScreen;
			}
		}

		public bool IsViewVisible {
			get {
				return IsViewLoaded && View.Window != null;
			}
		}

		public IIoCContainer IocContainer {
			get {
				return Verifier.Shared.App.IocContainer;
			}
		}

		public nint RowsInSection (UITableView tableView, nint section)
		{
			return applicantsList.Count;
		}
		[Export ("tableView:heightForRowAtIndexPath:")]
		public System.nfloat GetHeightForRow (UIKit.UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			return ApplicantTableCell.EstHeight;
		}
		public UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell ("applicant_cell") as ApplicantTableCell ?? new ApplicantTableCell ("applicant_cell");
			var applicant = applicantsList [indexPath.Row];
			cell.UpdateState (applicant);
			return cell;
		}
	}
}
