﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;
using CoreAnimation;
using CoreGraphics;

namespace Verifier.IOS
{
	[Register ("SatisfactionMeter"), DesignTimeVisible (true)]
	public class SatisfactionMeter:UIView
	{
		UIView baseCircle;
		UIView indicatorCircle;
		UILabel meterValueLabel;
		CAShapeLayer indicatorLayer;
		float _value;

		public float Value {
			get{ return _value; }
			set{ 
				meterValueLabel.Text = value.ToString();
				_value = value; 
			}
		}

		public SatisfactionMeter (IntPtr p) : base (p)
		{

		}

		public SatisfactionMeter () : base ()
		{
			initialize ();
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			initialize ();
		}

		private void initialize ()
		{
			baseCircle = new UIView {
				BackgroundColor = UIColor.DarkGray
			};
			indicatorCircle = new UIView {
				//BackgroundColor = Styles.ThemeColor.ColorWithAlpha (0.5f)
			};
			indicatorLayer = new CAShapeLayer ();
			indicatorCircle.Layer.AddSublayer (indicatorLayer);
			meterValueLabel = new UILabel {
				Text = "80%",
				TextColor = UIColor.White,
				Font = UIFont.BoldSystemFontOfSize (10),
				TextAlignment = UITextAlignment.Center
			};
			AddSubviews (new UIView[]{ baseCircle, indicatorCircle, meterValueLabel });
			this.DefineLayout (new string[] {
				"|-5-[base]-5-|",
				"|-0-[indicator]-0-|",
			}, "base", baseCircle, "indicator", indicatorCircle, "label", meterValueLabel);
			this.AddConstraints (new NSLayoutConstraint[] {
				NSLayoutConstraint.Create (baseCircle, NSLayoutAttribute.Width, NSLayoutRelation.Equal, baseCircle, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create (indicatorCircle, NSLayoutAttribute.Width, NSLayoutRelation.Equal, indicatorCircle, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create (baseCircle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create (baseCircle, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create (indicatorCircle, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create (indicatorCircle, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create (meterValueLabel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create (meterValueLabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterY, 1, 0),
				NSLayoutConstraint.Create (this, NSLayoutAttribute.Height, NSLayoutRelation.Equal, indicatorCircle, NSLayoutAttribute.Height, 1, 0),
			});
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			indicatorLayer.FillColor = Styles.ThemeColor.ColorWithAlpha (0.5f).CGColor;
			var path = new CGPath ();
			path.AddArc (Bounds.Width / 2, Bounds.Height / 2, Bounds.Height / 2, (float)Math.PI / 180 * (-290), (float)Math.PI / 180 * (0), false);
			path.AddLineToPoint (new CGPoint (Bounds.Width / 2, Bounds.Height / 2));
			indicatorLayer.Path = path;
			baseCircle.Layer.CornerRadius = baseCircle.Bounds.Height / 2;
		}
	}
}

