﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;
using Verifier.Core;
using System.Reactive.Linq;
using Splat;
namespace Verifier.IOS
{
	public struct JobCellState
	{
		public string Title;
		public string ThumbImageUrl;
		public string Address;
		public DateTime CreateDate;
		public DateTime VerificationDate;
		public JobStatus JobStatus;
		public ReportStatus ReportStatus;
		public int ApplicantCount;
	}

	public enum JobCellStyle
	{
		Default
	}

	[Register ("SimpleTableViewCell"), DesignTimeVisible (false)]
	public class JobListCell:UITableViewCell
	{
		public JobCellState State{ get; private set; }

		public void UpdateState (JobCellState newState)
		{
			JobTitle = newState.Title;
			subTitle.Text = newState.Address;
			if (cellStyle == JobCellStyle.Default) {
				DatePosted = "Job posted on "+newState.CreateDate.ToShortDateString ();
				ApplicantsCount = newState.ApplicantCount;
			}
			if (newState.ThumbImageUrl != State.ThumbImageUrl) {
				setThumbImage (newState.ThumbImageUrl);
			}
			setJobSatus (newState.JobStatus);
			State = newState;
		}
		private void setJobSatus(JobStatus status){
			switch (status) {
			case JobStatus.Active:
				statusIndicator.BackgroundColor = Styles.ThemeColor;
				break;
			case JobStatus.ReportPending:
				statusIndicator.BackgroundColor = UIColor.Yellow;
				break;
			case JobStatus.ReportSubmitted:
				statusIndicator.BackgroundColor = UIColor.Orange;
				break;
			case JobStatus.ReportReceived:
				statusIndicator.BackgroundColor = UIColor.Brown;
				break;
			case JobStatus.Completed:
				statusIndicator.BackgroundColor = UIColor.Gray;
				break;
			}
		}
		private async void setThumbImage(string url){
			var bitmap = await cacheManager.GetImage (url);
			thumbImageView.Image = bitmap.ToNative ();
		}
		ICacheManager cacheManager{
			get{
				return Verifier.Shared.App.IocContainer.Get<ICacheManager>();
			}
		}
		private UIView borderView;
		private UIView selectedBackgroundView;
		protected UIView column1;
		protected UIView column2;
		protected UIView column3;
		public UIImage ThumbImage {
			get {
				return thumbImageView.Image;
			}
			set {
				thumbImageView.Image = value;
			}
		}

		private UIImageView thumbImageView;

		public JobListCell (IntPtr handle) : base (handle)
		{
			
		}

		JobCellStyle cellStyle;

		public JobListCell (string reuseId, JobCellStyle cellStyle = JobCellStyle.Default) : base (UITableViewCellStyle.Default, reuseId)
		{
			this.cellStyle = cellStyle;
			initialize ();
		}

		public override void SetHighlighted (bool highlighted, bool animated)
		{
			var backupColor = statusIndicator.BackgroundColor;
			base.SetHighlighted (highlighted, animated);
			statusIndicator.BackgroundColor = backupColor;
		}

		public override void SetSelected (bool selected, bool animated)
		{
			var backupColor = statusIndicator.BackgroundColor;
			base.SetSelected (selected, animated);
			statusIndicator.BackgroundColor = backupColor;
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			initialize ();
		}

		private void initialize ()
		{
			addPadding ();
			addBorder ();
			adjustHighlightedAppearance ();
			initColumns ();
			layoutColumn1 ();
			layoutColumn2 ();
			layoutColumn3 ();
		}

		private void adjustHighlightedAppearance ()
		{
			selectedBackgroundView = new UIView {
				BackgroundColor = UIColor.FromRGB (243, 243, 243)
			};
			SelectedBackgroundView = selectedBackgroundView;
		}

		private void layoutColumn3 ()
		{
			var arrowRight = new UIImageView {
				Image = UIImage.FromBundle ("Icons/arrow-right.png"),
				ContentMode = UIViewContentMode.ScaleAspectFit
			};
			arrowRight.Layer.Opacity = 0.5f;
			column3.AddSubview (arrowRight);
			column3.DefineLayout ("|-20@1000-[arrow]-10@1000-|", "arrow", arrowRight);
			column3.DefineLayout ("V:|-10@1000-[arrow]-10@1000-|", "arrow", arrowRight);
		}

		private UILabel titleLabel;
		private UILabel subTitle;
		private UILabel dateLabel;
		private UILabel statusLabel;
		private UIView statusIndicator;
		private UILabel applicationCount;


		public String JobTitle {
			set {
				titleLabel.Text = value;
			}
		}

		public string DatePosted {
			set {
				dateLabel.Text = value;
			}
		}

		public float ApplicantsCount {
			set {
				applicationCount.Text = "Applicants: " + value.ToString ();
			}
		}

		private void layoutColumn2 ()
		{
			titleLabel = new UILabel {
				Text = "Title goes here",
				TextColor = UIColor.DarkTextColor,
				Font = UIFont.BoldSystemFontOfSize (13)
			};
			subTitle = new UILabel {
				Text = "9045, Los angales",
				TextColor = UIColor.LightGray,
				Font = UIFont.ItalicSystemFontOfSize (12)
			};
			dateLabel = new UILabel {
				Text = "Ad placed on 10/12/2015",
				TextColor = UIColor.LightGray,
				Font = UIFont.SystemFontOfSize (12)
			};
			statusLabel = new UILabel {
				Text = "Status",
				TextColor = UIColor.LightGray,
				Font = UIFont.SystemFontOfSize (12)
			};
			statusIndicator = new UIView {
				BackgroundColor = Styles.ThemeColor
			};
			statusIndicator.Layer.CornerRadius = 6;

			applicationCount = new UILabel {
				Text = "Applicants: 12",
				TextColor = UIColor.LightGray,
				Font = UIFont.SystemFontOfSize (12)
			};
			column2.AddSubviews (new UIView[] {
				titleLabel,
				subTitle,
				dateLabel,
				statusLabel,
				statusIndicator,
				applicationCount
			});
			column2.DefineLayout (new string[] {
				"|-0-[title]-0-|",
				"|-0-[subTitle]-0-|",
				"|-0-[date]-0-|",
				"|-0-[statusLabel]-5@1000-[statusView(12@1000)]-10@200-[applications]-0-|",
				"V:|-10-[title]-0-[subTitle]-8-[date]-0@200-[statusLabel]-8@1000-|",
				"V:[statusView(12)]-8-|",
				"V:[applications]-8-|",
			}, "title", titleLabel, "subTitle", subTitle, "date", dateLabel, "statusLabel", statusLabel, "statusView", statusIndicator, "applications", applicationCount);
		}

		private void layoutColumn1 ()
		{
			thumbImageView = new UIImageView {
				Image = UIImage.FromBundle ("Icons/car.png"),
				ContentMode = UIViewContentMode.ScaleAspectFit
			};
			column1.AddSubview (thumbImageView);
			column1.DefineLayout ("|-15-[image(80@200)]-15-|", "image", thumbImageView);
			column1.DefineLayout ("V:|-15-[image(80@200)]-15-|", "image", thumbImageView);
		}

		private void addPadding ()
		{
			this.DefineLayout ("|-10-[content]-10-|", "content", ContentView);
			this.DefineLayout ("V:|-0-[content]-0-|", "content", ContentView);
		}

		private void addBorder ()
		{
			borderView = new UIView ();
			borderView.BackgroundColor = UIColor.FromRGB (203, 203, 203);
			ContentView.AddSubview (borderView);
			ContentView.DefineLayout ("|-0-[border]-0-|", "border", borderView);
			ContentView.DefineLayout ("V:[border(1)]-0-|", "border", borderView);
		}

		private void initColumns ()
		{
			column1 = new UIView ();
			column2 = new UIView ();
			column3 = new UIView ();
			ContentView.AddSubviews (new UIView[]{ column1, column2, column3 });
			ContentView.DefineLayout (new string[] {
				"|-0-[column1]-0-[column2]-0-[column3]-0-|",
				"V:|-0-[column1]-0-|", 
				"V:|-0-[column2]-0-|",
				"V:|-0-[column3]-0-|"
			},
				"column1", column1, "column2", column2, "column3", column3, "border", borderView);
			setColumnWidthRatio (0.3f, 0.5f, 0.2f);
		}

		private void setColumnWidthRatio (float ratio1, float ratio2, float ratio3)
		{
			ContentView.AddConstraints (new NSLayoutConstraint[] {
				NSLayoutConstraint.Create (column1, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.Width, ratio1, 0),
				NSLayoutConstraint.Create (column2, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.Width, ratio2, 0),
				NSLayoutConstraint.Create (column3, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.Width, ratio3, 0)
			});
		}
	}
}

