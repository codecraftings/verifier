﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;
using Verifier.Core;
using Splat;
namespace Verifier.IOS
{
	[Register ("VerifierViewCell"), DesignTimeVisible(true)]
	public class ApplicantTableCell:UITableViewCell
	{
		private UIView borderView;
		private UIView selectedBackgroundView;
		protected UIView column1;
		protected UIView column2;
		protected UIView column3;
		public const float EstHeight = 100;
		public UIImage ThumbImage{
			get{
				return thumbImageView.Image;
			}
			set{
				thumbImageView.Image = Verifier.Shared.Util.CircularImage(value, 80, 80);
			}
		}
		private UIImageView thumbImageView;
		public static string KEY = "verifierViewCell";
		public JobApplicant lastState;
		public void UpdateState(JobApplicant applicant){
			titleLabel.Text = applicant.Profile.Name + "- " + applicant.Profile.Age + " years old";
			subTitle.Text = applicant.Profile.ProfileTitle;
			dateLabel.Text = "Verifier since: " + applicant.Profile.CreatedAt.ToShortDateString ();
			verificationCountLabel.Text = "Verifications done: " + applicant.Profile.JobCount;
			offerValueLabel.Text = "$" + applicant.Offer;
			satisfactionSatsMeter.Value = applicant.Profile.OverallSatisfaction;
			if (lastState!=null&&lastState.Profile.ProfilePic != applicant.Profile.ProfilePic) {
				setThumbImage (applicant.Profile.ProfilePic);
			}
		}

		async void setThumbImage (string url)
		{
			var img = await Verifier.Shared.App.IocContainer.Get<ICacheManager> ().GetImage (url);
			ThumbImage = img.ToNative();
		}

		public ApplicantTableCell (IntPtr handle) : base (handle)
		{

		}

		public ApplicantTableCell (string reuseId = "verifierViewCell"):base(UITableViewCellStyle.Default, reuseId)
		{
			initialize ();
		}

		public override void SetHighlighted (bool highlighted, bool animated)
		{
			UIView.Animate (0.2f, () => {
				if(highlighted)
					BackgroundColor = UIColor.DarkGray.ColorWithAlpha(0.1f);
				else
					BackgroundColor = UIColor.White;
			});
			//var backupColor = statusIndicator.BackgroundColor;
			//base.SetHighlighted (highlighted, animated);
			//statusIndicator.BackgroundColor = backupColor;
		}
		public override void SetSelected (bool selected, bool animated)
		{
			//var backupColor = statusIndicator.BackgroundColor;
			//base.SetSelected (selected, animated);
			//statusIndicator.BackgroundColor = backupColor;
		}
		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			initialize ();
		}
		private void initialize ()
		{
			addPadding ();
			addBorder ();
			adjustHighlightedAppearance ();
			initColumns ();
			layoutColumn1 ();
			layoutColumn2 ();
			layoutColumn3 ();
			layoutColumn4 ();
		}

		private void adjustHighlightedAppearance(){
			selectedBackgroundView = new UIView {
				BackgroundColor = UIColor.FromRGB (243, 243, 243)
			};
			SelectedBackgroundView = null;
			//SelectedBackgroundView = selectedBackgroundView;
		}
		UILabel offerValueLabel;
		private void layoutColumn4(){
			var icon = new UIImageView () {
				Image = UIImage.FromBundle("Icons/cash.png"),
				ContentMode = UIViewContentMode.ScaleAspectFit,
				ClipsToBounds = true
			};
			offerValueLabel = new UILabel {
				Text = "$100",
				TextAlignment = UITextAlignment.Center,
				Font = UIFont.SystemFontOfSize(13)
			};
			column4.AddSubviews (new UIView[]{ icon, offerValueLabel });
			column4.AddConstraints (new NSLayoutConstraint[] {
				NSLayoutConstraint.Create(icon, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, column4, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create(icon, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, column4, NSLayoutAttribute.CenterY, 1, -5)
			});
			column4.DefineLayout (new string[] {
				"[icon(30)]",
				"|-0-[label]-0-|",
				"V:|-(>=5)-[icon(30)]-2-[label]-(>=10)-|",
			}, "icon", icon, "label", offerValueLabel);
		}
		private SatisfactionMeter satisfactionSatsMeter;
		private void layoutColumn3(){
			satisfactionSatsMeter = new SatisfactionMeter ();
			var label = new UILabel {
				Text = "Satisfaction",
				TextAlignment = UITextAlignment.Center,
				Font = UIFont.SystemFontOfSize(10)
			};
			column3.AddSubview (satisfactionSatsMeter);
			column3.AddSubview (label);
			column3.DefineLayout ("|-15@1000-[meter]-10@1000-|", "meter", satisfactionSatsMeter);
			column3.DefineLayout ("|-0@1000-[label]-0@1000-|", "label", label);
			column3.DefineLayout ("V:|-10@200-[meter]-5-[label]-10@1000-|", "meter", satisfactionSatsMeter, "label", label);
		}
		private UILabel titleLabel;
		private UILabel subTitle;
		private UILabel dateLabel;
		private UILabel verificationCountLabel;

		public String JobTitle{
			set{
				titleLabel.Text = value;
			}
		}
		public string DatePosted{
			set{
				dateLabel.Text = value;
			}
		}
		public float VerificationsCount{
			set{
				verificationCountLabel.Text = "Verifications done " + value.ToString ();
			}
		}
		private void layoutColumn2(){
			titleLabel = new UILabel {
				Text = "Shakib Al Hasan",
				TextColor = UIColor.DarkTextColor,
				Font = UIFont.BoldSystemFontOfSize (13)
			};
			subTitle = new UILabel {
				Text = "Mechanical Engineer",
				TextColor = UIColor.LightGray,
				Font = UIFont.ItalicSystemFontOfSize(12)
			};
			dateLabel = new UILabel {
				Text = "Verifier since 10/12/2015",
				TextColor = UIColor.LightGray,
				Font = UIFont.SystemFontOfSize(12)
			};
			verificationCountLabel = new UILabel {
				Text = "Verifications done 3",
				TextColor = Styles.ThemeColor,
				Font = UIFont.SystemFontOfSize(12)
			};
			column2.AddSubviews (new UIView[] {
				titleLabel,
				subTitle,
				dateLabel,
				verificationCountLabel
			});
			column2.DefineLayout (new string[] {
				"|-0-[title]-0-|",
				"|-0-[subTitle]-0-|",
				"|-0-[date]-0-|",
				"|-0-[verificationCount]-0-|",
				"V:|-10-[title]-0-[subTitle]-8-[date]-0@200-[verificationCount]-8@1000-|",
			}, "title", titleLabel, "subTitle", subTitle, "date", dateLabel, "verificationCount", verificationCountLabel);
		}
		private void layoutColumn1(){
			thumbImageView = new UIImageView {
				Image = Verifier.Shared.Util.CircularImage(UIImage.FromBundle("Sample/sakib.jpg"), 80, 80),
				ClipsToBounds = true,
				ContentMode = UIViewContentMode.ScaleAspectFill
			};
			column1.AddSubview (thumbImageView);
			column1.DefineLayout ("|-5-[image(50@200)]-15-|", "image", thumbImageView);
			column1.DefineLayout ("V:|-15-[image(50@200)]-15-|", "image", thumbImageView);
			column1.AddConstraints (new NSLayoutConstraint[] {
				NSLayoutConstraint.Create(thumbImageView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, thumbImageView, NSLayoutAttribute.Height, 1, 0)
			});
		}
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			//thumbImageView.Layer.BorderColor = UIColor.Blue.CGColor;
			//thumbImageView.Layer.BorderWidth = 2;
		}
		private void addPadding(){
			this.DefineLayout ("|-2-[content]-2-|", "content", ContentView);
			this.DefineLayout ("V:|-0-[content]-0-|", "content", ContentView);
		}
		private void addBorder(){
			borderView = new UIView ();
			borderView.BackgroundColor = UIColor.FromRGB(203,203,203);
			ContentView.AddSubview (borderView);
			ContentView.DefineLayout ("|-0-[border]-0-|", "border", borderView);
			ContentView.DefineLayout ("V:[border(1)]-0-|", "border", borderView);
		}
		UIView column4 = new UIView();
		private void initColumns ()
		{
			column1 = new UIView ();
			column2 = new UIView ();
			column3 = new UIView ();
			column4 = new UIView ();
//			column4.BackgroundColor = UIColor.Black;
			ContentView.AddSubviews (new UIView[]{ column1, column2, column3, column4 });
			ContentView.DefineLayout (new string[] {
				"|-0-[column1]-5-[column2]-0-[column4]-0-[column3]-0-|",
				"V:|-0-[column1]-0-|", 
				"V:|-0-[column2]-0-|",
				"V:|-0-[column4]-0-|",
				"V:|-0-[column3]-0-|"
			},
				"column1", column1, "column2", column2, "column3", column3, "column4",column4, "border", borderView);
			setColumnWidthRatio (0.22f, 0.43f, 0.15f, 0.20f);
		}
		private void setColumnWidthRatio(float ratio1, float ratio2, float ratio4, float ratio3){
			ContentView.AddConstraints (new NSLayoutConstraint[] {
				NSLayoutConstraint.Create(column1, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.Width, ratio1,0),
				NSLayoutConstraint.Create(column2, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.Width, ratio2,0),
				NSLayoutConstraint.Create(column4, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.Width, ratio4,0),
				NSLayoutConstraint.Create(column3, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.Width, ratio3,0),
			});
		}
	}
}

