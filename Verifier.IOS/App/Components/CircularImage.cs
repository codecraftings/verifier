﻿using System;
using Foundation;
using System.ComponentModel;
using UIKit;

namespace Verifier.IOS
{
	[Register("CircularImage"), DesignTimeVisible(true)]
	public class CircularImage:UIImageView
	{
		public CircularImage (IntPtr p):base(p)
		{
		}
		public CircularImage():base(){
			initialize ();
		}
		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			initialize ();
		} 

		float getRadius ()
		{
			return Bounds.Width > Bounds.Height ? (float)Bounds.Height : (float)Bounds.Width;
		}

//		public override UIImage Image {
//			get {
//				return base.Image;
//			}
//			set {
//				base.Image = value;
////				var c = getRadius ();
////				base.Image = Verifier.Shared.Util.CircularImage (value, c, c);
//			}
//		}
		//override 
		void initialize(){
			//if(Image!=null)
			//	Image = Image;
			ContentMode = UIViewContentMode.ScaleAspectFill;
			ClipsToBounds = true;
		}
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			Layer.BorderWidth = 0.1f;
			Layer.BorderColor = UIColor.Clear.CGColor;
			Layer.CornerRadius = Frame.Size.Width / 2;
		}
	}
}

