﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;

namespace Verifier.IOS
{
	[Register ("SimpleSlickButton"), DesignTimeVisible (true)]
	public class SimpleSlickButton:UIButton
	{
		private UIColor hoverBackground;

		[Export ("HoverBackground"), Browsable (true)]
		public UIColor HoverBackground { 
			get {
				if (hoverBackground == null||hoverBackground==UIColor.Clear) {
					return UIColor.Clear;
				}
				return hoverBackground.ColorWithAlpha (0.2f);
			}
			set {
				hoverBackground = value;
			}
		}

		public SimpleSlickButton (IntPtr p) : base (p)
		{

		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			Layer.CornerRadius = 3;

		}

		private void highlightState ()
		{
			UIView.Animate (0.3f, () => {
				BackgroundColor = HoverBackground;
				//SetTitleColor(Styles.ThemeColor, UIControlState.Highlighted);
				Layer.Opacity = 1;
			});
		}

		private void normalState ()
		{
			UIView.Animate (0.3f, () => {
				BackgroundColor = UIColor.Clear;
			});
		}

		public override void TouchesBegan (Foundation.NSSet touches, UIEvent evt)
		{
			highlightState ();
			base.TouchesBegan (touches, evt);
		}

		public override void TouchesEnded (Foundation.NSSet touches, UIEvent evt)
		{
			normalState ();
			base.TouchesEnded (touches, evt);
		}

		public override void TouchesCancelled (Foundation.NSSet touches, UIEvent evt)
		{
			normalState ();
			base.TouchesCancelled (touches, evt);
		}
	}
}

