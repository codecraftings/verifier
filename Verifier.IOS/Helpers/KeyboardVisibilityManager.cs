﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;

namespace Verifier.IOS
{
	public class KeyboardVisibilityManager
	{
		public UIScrollView ScrollContainer;
		public UIView MainView;

		public KeyboardVisibilityManager (UIScrollView scrollview, UIView mainView)
		{
			MainView = mainView;
			if (scrollview == null) {
				scrollview = addScrollViewInBetween (mainView);
			}
			ScrollContainer = scrollview;
		}

		static UIScrollView addScrollViewInBetween (UIView mainView)
		{
			//not tested properly
			UIScrollView scrollview;
			var containerView = new UIView ();
			scrollview = new UIScrollView ();
			foreach (var view in mainView.Subviews) {
				view.RemoveFromSuperview ();
				containerView.AddSubview (view);
			}
			scrollview.AddSubview (containerView as UIView);
			scrollview.DefineLayout ("|-0-[view]-0-|", "view", containerView);
			scrollview.DefineLayout ("V:|-0-[view]-0-|", "view", containerView);
			mainView.AddSubview (scrollview);
			mainView.DefineLayout ("|-0-[scroll]-0-|", "scroll", scrollview);
			mainView.DefineLayout ("V:|-0-[scroll]-0-|", "scroll", scrollview);
			return scrollview;
		}

		public void KeyboardVisible (NSNotification notification)
		{
			CGSize keyboardInfo = UIKeyboard.FrameEndFromNotification (notification).Size;
			var inset = new UIEdgeInsets (0, 0, keyboardInfo.Height, 0);
			ScrollContainer.ContentInset = inset;
			ScrollContainer.ScrollIndicatorInsets = inset;
			var activeField = ScrollContainer.FindFirstResponder ();
			if (activeField != null)
				ScrollContainer.ScrollRectToVisible (activeField.Frame, true);
		}

		public void KeyboardHidden (NSNotification notification)
		{
			ScrollContainer.ContentInset = UIEdgeInsets.Zero;
			ScrollContainer.ScrollIndicatorInsets = UIEdgeInsets.Zero;
		}

		protected void DismissKeyboardOnBackgroundTap ()
		{
			// Add gesture recognizer to hide keyboard
			var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
			tap.AddTarget (() => MainView.EndEditing (true));
			MainView.AddGestureRecognizer (tap);
		}

		public void SetUp ()
		{
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, KeyboardVisible);
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, KeyboardHidden);
			DismissKeyboardOnBackgroundTap ();
		}
	}

	public static class UIViewExtension
	{
		public static UIView FindFirstResponder (this UIView view)
		{
			if (view.IsFirstResponder) {
				return view;
			}
			foreach (UIView subView in view.Subviews) {
				var firstResponder = subView.FindFirstResponder ();
				if (firstResponder != null)
					return firstResponder;
			}
			return null;
		}
	}
}

