using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Verifier.IOS
{
	partial class ApplicantViewController : UIViewController
	{
		public ApplicantViewController (IntPtr handle) : base (handle)
		{
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			applicationDetails.Layer.BorderColor = Styles.ThemeColor.CGColor;
			applicationDetails.Layer.BorderWidth = 1f;
			applicationDetails.Layer.CornerRadius = 3f;
		}
	}
}
