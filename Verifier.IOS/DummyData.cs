﻿using System;

namespace Verifier.IOS
{
	public class DummyData
	{
		public static Tuple<string, string, string, int>[] Jobs;
		static DummyData() {
			Jobs = new Tuple<string, string, string, int>[] {
				Tuple.Create("1775 Marcedes Bench, gas engine", "10/11/2015", "Icons/car.png", 10),
				Tuple.Create("Truck Verifier, gas engine", "20/11/2015", "Icons/truck.png", 20),
				Tuple.Create("Real estate verifier", "30/12/2015", "Icons/estate.png", 10),
				Tuple.Create("1775 Marcedes Bench, gas engine", "10/11/2015", "Icons/car.png", 10),
				Tuple.Create("Product Verifier", "10/11/2015", "Icons/product.png", 23),
				Tuple.Create("1775 Marcedes Bench, gas engine", "10/11/2015", "Icons/car.png", 10),
				Tuple.Create("Real estate verifier", "30/12/2015", "Icons/estate.png", 10),
				Tuple.Create("Truck Verifier, gas engine", "20/11/2015", "Icons/truck.png", 20),
			};
		}
		public DummyData ()
		{
		}
	}
}

