﻿using System;
using UIKit;
using CoreGraphics;
using Verifier.Core;
namespace Verifier.IOS
{
	public class NavigationDrawer:UIView
	{
		UIView parentView;
		CGRect _frameWhenVisible;
		CGRect _frameWhenHidden;
		UIView drawerPanel;

		UIView headerSectionContainer;

		UIImageView userAvaterView;
		UILabel userNameLabel;

		UITableView menuContainer;

		public static Tuple<string, string, UIColor, bool, Action>[] NavigationMenus;

		MainNavigationController navController;
		SessionActionsCreator sessionActionCreator;
		public NavigationDrawer (UIView parentView, MainNavigationController navController)
		{
			this.navController = navController;
			this.parentView = parentView;
			sessionActionCreator = Verifier.Shared.App.IocContainer.Get<SessionActionsCreator> ();
			NavigationMenus = new Tuple<string, string, UIColor, bool, Action>[] {
				Tuple.Create("Home", "Icons/home.png", Styles.ThemeColor, true, default(Action)),
				Tuple.Create("Messages", "Icons/message.png", Styles.ThemeColor, false, default(Action)),
				Tuple.Create("Account", "Icons/account.png", Styles.ThemeColor, false, default(Action)),
				Tuple.Create("Settings", "Icons/setting.png", Styles.ThemeColor, false, default(Action)),
				Tuple.Create("Logout", "Icons/logout.png", Styles.ThemeColor, false, new Action(handleLogoutClicked)),
			};
			Initialize ();	
		}
		private void handleLogoutClicked(){
			navController.ShowConfirmAlert ("Are you sure you want to logout?", sessionActionCreator.TryLogout, () => {
			});
		}
		private void Initialize ()
		{
			Frame = new CGRect (0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height);
			Hidden = true;
			drawerPanel = new UIView ();
			drawerPanel.BackgroundColor = UIColor.White;
			AddSubview (drawerPanel);
			_frameWhenVisible = new CoreGraphics.CGRect (0, 0, 250, UIScreen.MainScreen.Bounds.Height);
			_frameWhenHidden = new CoreGraphics.CGRect (-250, 0, 250, UIScreen.MainScreen.Bounds.Height);
			drawerPanel.Frame = _frameWhenHidden;
			Opaque = true;

			drawerPanel.Layer.ShadowOffset = new CGSize (1, Styles.StatusBarHeight);
			drawerPanel.Layer.ShadowRadius = 2f;
			drawerPanel.Layer.ShadowPath = UIBezierPath.FromRect (_frameWhenVisible).CGPath;
			drawerPanel.Layer.ShadowColor = UIColor.Black.CGColor;
			drawerPanel.Layer.MasksToBounds = false;
			drawerPanel.Layer.ShadowOpacity = 0.3f;

			initializeHeader ();
			initializeMenuContainer ();
		}

		private void initializeMenuContainer(){
			var navigationMenuAdapter = new NavigationMenuAdapter ();
			menuContainer = new UITableView {
				SeparatorStyle = UITableViewCellSeparatorStyle.None,
				DataSource = navigationMenuAdapter,
				Delegate = navigationMenuAdapter
			};
			drawerPanel.AddSubview (menuContainer);
			drawerPanel.DefineLayout ("|-0-[container]-0-|", "container", menuContainer);
			drawerPanel.DefineLayout ("V:[header]-20-[container]-0-|","header", headerSectionContainer, "container", menuContainer);


		}
		private void initializeHeader ()
		{
			headerSectionContainer = new UIView {
				BackgroundColor = Styles.ThemeColor
			};
			var statusBar = new UIView {
				BackgroundColor = UIColor.Black.ColorWithAlpha (0.2f)
			};



			drawerPanel.AddSubviews (new UIView[] {
				headerSectionContainer,
				statusBar,
			});


			drawerPanel.DefineLayout (
				new string[] {
					"|-0-[header]-0-|",
					"V:|-0-[header(150)]",
					"|-0-[status]-0-|",
					"V:|-0-[status(" + Styles.StatusBarHeight + ")]"
				}, "header", headerSectionContainer, "status", statusBar);

			userAvaterView = new UIImageView {
				Image = UIImage.FromBundle ("avater_man.png"),
				BackgroundColor = UIColor.White,
				ContentMode = UIViewContentMode.ScaleAspectFit
			};
			userNameLabel = new UILabel {
				Text = "Mahfuz Khan",
				TextColor = UIColor.White,
				TextAlignment = UITextAlignment.Center,
				Font = UIFont.SystemFontOfSize(14)
			};
			headerSectionContainer.AddSubviews (new UIView[] {
				userAvaterView,
				userNameLabel
			});
			headerSectionContainer.DefineLayout (new string[] {
				"|-20-[avater]-20-|",
				"|-20-[label]-20-|",
				"V:|-20-[avater]-5-[label]-5-|"
			}, "avater", userAvaterView, "label", userNameLabel);
			drawerPanel.AddConstraints (new NSLayoutConstraint[] {
				NSLayoutConstraint.Create (userAvaterView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, userAvaterView, NSLayoutAttribute.Height, 1, 0),
				NSLayoutConstraint.Create (userAvaterView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.Width, 1f, 70),
				NSLayoutConstraint.Create (userAvaterView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, headerSectionContainer, NSLayoutAttribute.CenterX, 1, 0),
				NSLayoutConstraint.Create (userAvaterView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, headerSectionContainer, NSLayoutAttribute.CenterY, 1, 0),

			});
		}

		public override void TouchesBegan (Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			var touch = touches.AnyObject as UITouch;
			if (touch != null) {
				if (!drawerPanel.Frame.Contains (touch.LocationInView (this))) {
					Hide ();
				}
			}
		}


		public void Show ()
		{
			parentView.AddSubview (this);
			Hidden = false;

			BackgroundColor = UIColor.Black.ColorWithAlpha (0f);
			Animate (0.15f, 0, UIViewAnimationOptions.CurveEaseOut, () => {
				drawerPanel.Frame = _frameWhenVisible;
				BackgroundColor = UIColor.Black.ColorWithAlpha (0.2f);
			}, () => {
			});
		}

		public void Hide ()
		{
			Animate (0.15f, 0, UIViewAnimationOptions.CurveEaseOut, () => {
				drawerPanel.Frame = _frameWhenHidden;
			}, () => {
				Hidden = true;
				this.RemoveFromSuperview ();
			});
		}
	}

	class NavigationMenuAdapter:UITableViewDataSource,IUITableViewDelegate{
		[Foundation.Export ("tableView:didSelectRowAtIndexPath:")]
		public void RowSelected (UIKit.UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			var menuItem = NavigationDrawer.NavigationMenus [indexPath.Row];
			if (menuItem.Item5 != null) {
				menuItem.Item5.Invoke ();
			}
		}
		public override nint RowsInSection (UITableView tableView, nint section)
		{
			return NavigationDrawer.NavigationMenus.Length;
		}
		public override UITableViewCell GetCell (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			var menuItem = NavigationDrawer.NavigationMenus [indexPath.Row];
			var cell = new NavigationMenuItem ();
			cell.Label = menuItem.Item1;
			return cell;
		}
	}

	class NavigationMenuItem:UITableViewCell{
		public string Label{
			get{
				return menuLabel.Text;
			}
			set{
				menuLabel.Text = value;
			}
		}
		private UILabel menuLabel;
		public NavigationMenuItem(){
			initialize ();
		}
		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			initialize ();
		}
		private void initialize(){
			menuLabel = new UILabel {
				Text = "Menu Label",
				Font = UIFont.SystemFontOfSize(13)
			};
			ContentView.AddSubview(menuLabel);
			ContentView.DefineLayout(new string[]{
				"|-20-[label]-0-|",
				"V:|-5-[label]-5-|"
			}, "label", menuLabel);
		}
	}
}

