using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Verifier.IOS
{
	partial class GlobalNavigationBar : UINavigationBar
	{
		private UIImageView logoView;
		private static UIButton menuButton;
		private UIButton plusButton;
		public GlobalNavigationBar (IntPtr handle) : base (handle)
		{
		}
		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			TintColor = UIColor.White;
			logoView = new UIImageView {
				Image = UIImage.FromBundle ("Logo/logo-nav.png"),
				ContentMode = UIViewContentMode.ScaleAspectFit
			};
			var h = Bounds.Height - 15;
			var w = logoView.IntrinsicContentSize.Width / logoView.IntrinsicContentSize.Height * h;
			logoView.Frame = new CoreGraphics.CGRect ((UIScreen.MainScreen.Bounds.Width - w) / 2, (Bounds.Height - h)/2, w, h);
			AddSubview (logoView);
			plusButton = new UIButton (UIButtonType.System);
			plusButton.SetImage (UIImage.FromBundle ("Icons/plus.png"), UIControlState.Normal);
			plusButton.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
			menuButton = new UIButton (UIButtonType.System);
			menuButton.SetImage (UIImage.FromBundle ("Icons/hamburger.png"), UIControlState.Normal);
			menuButton.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
			AddSubview (menuButton);
			AddSubview (plusButton);

			var statusBarOverlay = new UIView {
				BackgroundColor = UIColor.Black.ColorWithAlpha (0.1f)
			};
			AddSubview (statusBarOverlay);
			this.DefineLayout (new string[] {
				"|-0-[status]-0-|",
				"V:|-(-20)-[status("+Styles.StatusBarHeight+")]",
				"[plus(35)]-5-|",
				"|-5-[menu(35)]",
				"V:|-10-[menu]-10-|", 
				"V:|-10-[plus]-10-|"
			}, "plus", plusButton, "menu", menuButton, "status", statusBarOverlay);
			menuButton.TouchUpInside += (sender, e) => MainNavigationController.ShowDrawerMenu();

		}

		public static void HideMenuButton(){
			menuButton.Hidden = true;
		}
		public static void ShowMenuButton(){
			menuButton.Hidden = false;
		}
		public override void PushNavigationItem (UINavigationItem item, bool animated)
		{
			base.PushNavigationItem (item, animated);
		}
		void HandleMenuButtonTapped (object sender, EventArgs e)
		{
					
		}
	}
}
