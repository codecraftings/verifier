using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using Verifier.Core;
using System.Collections.Generic;

namespace Verifier.IOS
{
	public partial class MainNavigationController : UINavigationController, IViewController
	{
		public NavigationStore.Screen ScreenIdentifier {
			get {
				throw new NotImplementedException ();
			}
		}

		public void RenderState ()
		{
			if (navStore.State.CurrentScreen.StoryBoardId != CurrentScreen.StoryBoardId) {
				drawerMenu.Hide ();
				CurrentScreen = navStore.State.CurrentScreen;
				var view = findOrCreateView (CurrentScreen);
				if (view != null)
					this.PopToViewController (view, true);
				else {
					view = Storyboard.InstantiateViewController (CurrentScreen.StoryBoardId);
					this.PushViewController (view, true);
				}
					
			}
		}
		private UIViewController findOrCreateView(NavigationStore.Screen screenObj){
			UIViewController result = null;
			foreach (var view in this.ViewControllers) {
				var v = view as IViewController;
				if (v != null&&v.ScreenIdentifier.StoryBoardId==screenObj.StoryBoardId) {
					result = view;
				}
			}
			return result;
		}
		public bool IsViewVisible {
			get {
				return true;
			}
		}

		public IIoCContainer IocContainer {
			get {
				return Verifier.Shared.App.IocContainer;
			}
		}

		private static NavigationDrawer drawerMenu;
		IStoreManager storeManager;
		NavigationStore navStore;
		NavigationStore.Screen CurrentScreen;
		public MainNavigationController (IntPtr handle) : base (handle)
		{
			storeManager = IocContainer.Get<IStoreManager> ();
			navStore = storeManager.Bind<NavigationStore> (this);
			CurrentScreen = navStore.State.CurrentScreen;
		}
		public override UIViewController PopViewController (bool animated)
		{
			//var v = TopViewController as IViewController;
			var view = base.PopViewController (animated);
			var v = view as IViewController;
			if (v != null && v.ScreenIdentifier.StoryBoardId == CurrentScreen.StoryBoardId) {
				CurrentScreen = new NavigationStore.Screen ();
			}
			return view;
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			drawerMenu = new NavigationDrawer (View, this);
			View.AddSubview (drawerMenu);
//
//			var status = new UIView {
//
//				BackgroundColor = UIColor.Black.ColorWithAlpha (0.2f)
//			};
//			View.AddSubview (status);
//			View.DefineLayout ("|-0-[status]-0-|", "status", status);
//			View.DefineLayout ("V:|-0-[status(20)]", "status", status);

		}



		public static void ShowDrawerMenu(){
			drawerMenu.Show ();
		}



		public void Alert (string message, string title = "Alert")
		{
			try {
				AlertNew (message, title);
			} catch {
				var alert = new UIAlertView (title, message, null, "Ok");
				alert.Show ();
			}

		}

		public void AlertNew (string message, string title)
		{
			var alert = UIAlertController.Create (title, message, UIAlertControllerStyle.Alert);
			alert.AddAction (UIAlertAction.Create ("Ok", UIAlertActionStyle.Cancel, null));
			PresentViewController (alert, true, null);
		}

		public void ShowConfirmAlert (string message, Action accepted, Action rejected)
		{
			try {
				confirmAlertNew (message, accepted, rejected);
			} catch {
				confirmAlertOld (message, accepted, rejected);
			}
		}

		private void confirmAlertOld (string message, Action accepted, Action rejected)
		{
			var alert = new UIAlertView ("Confirm", message, null, "Yes", "No");
			alert.Clicked += (object sender, UIButtonEventArgs e) => {
				if (e.ButtonIndex == alert.CancelButtonIndex) {
					if (accepted != null)
						accepted.Invoke ();
				} else {
					if (rejected != null)
						rejected.Invoke ();
				}
			};
			alert.Show ();
		}

		private void confirmAlertNew (string message, Action accepted, Action rejected)
		{
			var alert = UIAlertController.Create ("Confirm", message, UIAlertControllerStyle.Alert);
			alert.AddAction (UIAlertAction.Create ("Yes", UIAlertActionStyle.Default, ii => {
				if (accepted != null) {
					accepted.Invoke ();
				}
			}));
			alert.AddAction (UIAlertAction.Create ("No", UIAlertActionStyle.Default, ii => {
				if (rejected != null) {
					rejected.Invoke ();
				}
			}));
			PresentViewController (alert, true, null);
		}

		public void ShowActionSheet (Dictionary<string, Action> actions, string title)
		{
			try {
				actionSheetNew (actions, title);
			} catch {
				actionSheetOld (actions, title);
			}
		}

		private void actionSheetOld (Dictionary<string, Action> actions, string title)
		{
			var sheet = new UIActionSheet (title);
			List<Action> handlers = new List<Action> ();
			foreach (var action in actions) {
				sheet.AddButton (action.Key);
				handlers.Add (action.Value);
			}
			sheet.AddButton ("Cancel");
			sheet.Clicked += (object sender, UIButtonEventArgs e) => {

				if ((int)e.ButtonIndex < handlers.Count && handlers [(int)e.ButtonIndex] != null) {
					handlers [(int)e.ButtonIndex].Invoke ();
				}
			};
			sheet.ShowInView (View);
		}

		private void actionSheetNew (Dictionary<string, Action> actions, string title)
		{
			var sheet = UIAlertController.Create (title, null, UIAlertControllerStyle.ActionSheet);
			foreach (var action in actions) {
				sheet.AddAction (UIAlertAction.Create (action.Key, UIAlertActionStyle.Default, dd => action.Value.Invoke ()));
			}
			sheet.AddAction (UIAlertAction.Create ("Cancel", UIAlertActionStyle.Cancel, null));
			PresentViewController (sheet, true, null);
		}
	}
}
