﻿using System;
using UIKit;

namespace Verifier.IOS
{
	public static class Styles
	{
		public static UIColor ThemeColor = UIColor.FromRGB(86,189, 62);
		public static float StatusBarHeight = 20;
	}
}

