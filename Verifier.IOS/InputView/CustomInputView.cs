﻿using System;
using UIKit;
using System.ComponentModel;
using Foundation;

namespace Verifier.IOS
{
	[Register("CustomInputView"), DesignTimeVisible(true)]
	public class CustomInputView:UIView
	{
		public CustomInputView (IntPtr p):base(p)
		{
		}
		public CustomInputView(){
			initialize ();
		}
		[Export("InputLabel"), Browsable(true)]
		public String InputLabel{ get; set; }

		[Export("InputIcon"), Browsable(true)]
		public String InputIcon{ get; set;}

		public string Value{
			get{
				return inputView.Text;
			}
			set{
				inputView.Text = value;
			}
		}

		private CustomInputView nextField;
		private Action doneAction;
		private UIImageView iconContainer;
		private UITextField inputView;
		private UIView borderView;
		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			initialize ();
		}
		public void SetNextResponder(CustomInputView field){
			inputView.ReturnKeyType = UIReturnKeyType.Next;
			nextField = field;
		}
		public void SetDoneAction(Action action){
			inputView.ReturnKeyType = UIReturnKeyType.Done;
			doneAction = action;
		}
		public void MarkAsSecureField(){
			inputView.SecureTextEntry = true;
		}
		public void SetKeyboardType(UIKeyboardType keyboardType){
			inputView.KeyboardType = keyboardType;
		}
		public void GrabTheKeyboard(){
			inputView.BecomeFirstResponder ();
		}
		private void initialize(){
			if (InputLabel == null) {
				InputLabel = "Input Label";
			}
			addBorderBottom ();
			if (InputIcon != null) {
				addIcon ();
			}

			addInputBox ();
		}

		private void addBorderBottom(){
			borderView = new UIView ();
			borderView.BackgroundColor = UIColor.LightGray;
			AddSubview (borderView);
			this.DefineLayout (new string[]{"V:[view(1)]-0-|","|-0-[view]-0-|"}, "view", borderView);
		}

		private void addIcon(){
			iconContainer = new UIImageView {
				Image = UIImage.FromBundle (InputIcon),
				ContentMode = UIViewContentMode.ScaleAspectFit,
			};
			AddSubview (iconContainer);
			this.DefineLayout ("|-8-[icon]", "icon", iconContainer);
			this.DefineLayout ("V:|-8-[icon]-8-|", "icon", iconContainer);
			this.AddConstraint (NSLayoutConstraint.Create (iconContainer, NSLayoutAttribute.Width, NSLayoutRelation.Equal, iconContainer, NSLayoutAttribute.Height, 1, 0));
		}

		private void addInputBox(){
			inputView = new UITextField ();
			inputView.Placeholder = InputLabel;
			inputView.ShouldReturn = HandleReturnKey;
			AddSubview (inputView);
			if (iconContainer != null) {
				this.DefineLayout ("[icon]-10-[input]-5-|", "icon", iconContainer, "input", inputView);
			} else {
				this.DefineLayout ("|-10-[input]-5-|", "input", inputView);
			}
			this.DefineLayout ("V:|-5-[input]-5-[border]", "input", inputView, "border", borderView);
		}

		public bool HandleReturnKey(UITextField input){
			input.ResignFirstResponder ();
			if (nextField != null) {
				nextField.GrabTheKeyboard ();
			}
			if (doneAction != null) {
				doneAction.Invoke ();
			}
			return false;
		}

	}
}

