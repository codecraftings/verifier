﻿namespace Verifier.Shared
{
    /// <summary>
    /// Animation transition : entrance or ending.
    /// </summary>
    public enum AnimationTransition
    {
        In,
        Out,
    }
}

