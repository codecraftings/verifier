﻿namespace Verifier.Shared
{
    /// <summary>
    /// Animation movement direction.
    /// </summary>
    public enum AnimationDirection
    {
        None,
        Up,
        Down,
        Left,
        Right,
    }
}

