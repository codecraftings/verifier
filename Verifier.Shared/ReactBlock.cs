﻿using System;

namespace Verifier.Shared
{
	public class ReactBlock
	{
		Action actionBlock;
		object[] lastArgs;
		private ReactBlock (Action actionBlock)
		{
			this.actionBlock = actionBlock;
			lastArgs = new object[]{ };
		}
		public static ReactBlock Register(Action actionBlock){
			return new ReactBlock (actionBlock);
		}
		public void ConditionalInvoke(bool condition){
			if(condition)
				Invoke (new Object[] { condition });
		}
		public void Invoke(params object[] args){
			var shouldInvoke = false;
			if (args.Length != lastArgs.Length) {
				shouldInvoke = true;
			} else {
				for (var i = 0; i < args.Length; i++) {
					if (hasChanged (args [i], lastArgs [i]))
						shouldInvoke = true;
				}
			}
			if (shouldInvoke) {
				lastArgs = args;
				actionBlock.Invoke ();
			}
		}
		private static bool hasChanged(object currentState, object previousState){
			if (currentState != null)
				return !currentState.Equals (previousState);
			else if (previousState != null)
				return !previousState.Equals (currentState);
			else
				return false;
		}
	}
}

