﻿using System;
using Verifier.Core;
using System.Threading;


namespace Verifier.Shared
{
	public class ThreadManager:IThreadManager
	{
		public ThreadManager ()
		{
		}

		#region IThreadManager implementation

		public object StartThread (Action runBlock)
		{
			var thread = new Thread (runBlock.Invoke);
			thread.IsBackground = true;
			thread.Start ();
			return thread;
		}

		public void RunOnMainThread (Action runBlock)
		{
			new Foundation.NSObject ().InvokeOnMainThread (runBlock);
		}

		#endregion
	}
}

