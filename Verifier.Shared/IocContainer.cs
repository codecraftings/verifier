﻿using System;
using Verifier.Core;
using Ninject;


namespace Verifier.Shared
{
	public class IocContainer:IIoCContainer
	{
		readonly IKernel Kernel;
		public IocContainer ()
		{
			Kernel = new StandardKernel (new Ninject.Modules.INinjectModule[] {
				new NinjectBindings ()
			});
		}

		public void Bind (Type type1, Type type2, bool singleton = false)
		{
			Ninject.Syntax.IBindingWhenInNamedWithOrOnSyntax<object> binding;
			if (type2 == null)
				binding = Kernel.Bind (type1).ToSelf ();
			else
				binding = Kernel.Bind (type1).To (type2);
			if (singleton)
				binding.InSingletonScope ();
		}
		public object Get (Type type)
		{
			return Kernel.Get (type);
		}

		public T Get<T> ()
		{
			return Kernel.Get<T> ();
		}

	}
}

