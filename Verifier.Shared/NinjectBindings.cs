﻿using System;
using Verifier.Core;
namespace Verifier.Shared
{
	public class NinjectBindings:Ninject.Modules.NinjectModule
	{
		public NinjectBindings ()
		{
		}
		public override void Load ()
		{
			Bind<IRestClient> ().To<Verifier.Shared.DummyRestClient> ();
			Bind<ICacheManager> ().To<CacheManager> ().InSingletonScope();
			Bind<IThreadManager> ().To<ThreadManager> ();
			Bind<IStoreManager> ().To<StoreManager> ().InSingletonScope();
		}
	}
}

