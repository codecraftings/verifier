﻿using System;
using System.Threading.Tasks;
using Verifier.Core;
using Akavache;
using System.Reactive.Linq;
using System.Collections.Generic;
using System.Text;


namespace Verifier.Shared
{
	public class DummyRestClient:IRestClient
	{
		ICacheManager cacheManager;

		public DummyRestClient (ICacheManager cacheManager)
		{
			this.cacheManager = cacheManager;
			//BlobCache.ApplicationName = "DummyRestClient";
		}

		#region IRestClient implementation

		public IRestRequest CreateRequest (string apiPath, string httpMethod)
		{
			return new DummyRestRequest (apiPath, httpMethod);
		}

		public T Execute<T> (IRestRequest request)
		{
			throw new NotImplementedException ();
		}

		public Task<T> ExecuteAsync<T> (IRestRequest request)
		{
			return Task.Run<T> (async () => {
				await Task.Delay(1000);
				var o = await getDummyResponse (request as DummyRestRequest);
				return (T)o;
			});
		}

		#endregion

		async Task<List<TokenData>> getTokens ()
		{
			return await this.cacheManager.Get<List<TokenData>> (FakeSeeder.KeyTokens);
			//return await BlobCache.LocalMachine.GetOrCreateObject<List<TokenData>> (FakeSeeder.KeyTokens, () => new List<TokenData> ());
		}

		private async Task<object> getDummyResponse(DummyRestRequest request){
			object response = null;
			switch (request.ResourcePath) {
			case "/logout":
				response = new SuccessResponse {
					Message = "success",
					StatusCode = 200
				};
				break;
			case "/login":
				response = await login (request);
				break;
			case "/signup":
				response = await singup (request);
				break;
			case "/users/{id}":
				response = await getUser (request);
				break;
			case "/users/{id}/jobs":
				response = await getJobsPosted (request);
				break;
			case "/jobs/{id}":
				response = await getJob (request);
				break;
			case "jobs/search":
				response = await findJob (request);
				break;
			}

			if (response == null) {
				throw new Exception ("Invalid Request");
			}
			//BlobCache.LocalMachine.Flush ();
			return response;
		}


		async Task<object> getJob (DummyRestRequest request)
		{
			var jobs = await cacheManager.Get<List<Job>> (FakeSeeder.KeyJobs);
			var id = request.getParam<long> ("id");
			return jobs.Find (job => job.ID == id);
		}

		async Task<object> findJob (DummyRestRequest request)
		{
			var filter = request.getParam<JobFilter> ("filter");
			var jobs = await cacheManager.Get<List<Job>> (FakeSeeder.KeyJobs);
			return jobs;
		}
		async Task<object> getJobsPosted (DummyRestRequest request)
		{
			var buyerId = request.getParam<long> ("id");
			var filter = request.getParam<JobFilter> ("filter");
			var jobs = await cacheManager.Get<List<Job>> (FakeSeeder.KeyJobs);
			return jobs.FindAll (job => job.Buyer.ID == buyerId);
		}
		async Task<object> getUser(DummyRestRequest request){
			var users = await cacheManager.Get<List<User>> (FakeSeeder.KeyUsers);
			var id = request.getParam<long> ("id");
			return users.Find (u => u.ID == id);
		}

		private async Task<object> login(DummyRestRequest request){
			var tokens = await getTokens ();
			var email = request.getParam<string> ("email");
			var password = request.getParam<string> ("password");
			foreach (var token in tokens) {
				if (token.Email == email && token.Password == password) {
					return new AuthResponse(){
						AccessToken = token.AccessToken,
						UserData = token.User,
						StatusCode = 200
					};
				}
			}
			throw new Exception ("Email password does not match");
		}
		private async Task<object> singup(DummyRestRequest request){
			var tokens2 = await getTokens ();
			var name = request.getParam<string> ("name");
			var email = request.getParam<string> ("email");
			var type = request.getParam<UserType> ("user_type");
			var password = request.getParam<string> ("password");
			var confPassword = request.getParam<string> ("password_confirmation");
			if (password != confPassword) {
				throw new Exception ("Password does not match");
			}

			var user = new User () {
				ID = new Random((int)DateTime.UtcNow.ToFileTime()).Next(100, 10000000),
				Name = name,
				Email = email,
				Type = type
			};
			var accessToken = Guid.NewGuid ().ToString ().Substring (0, 20);
			var tokenData = new TokenData {
				AccessToken = accessToken,
				Email = email,
				Password = password,
				User = user
			};
			tokens2.Add (tokenData);
			//await BlobCache.LocalMachine.InsertObject<List<TokenData>> (FakeSeeder.KeyTokens, tokens2);
			await cacheManager.Put<List<TokenData>>(FakeSeeder.KeyTokens, tokens2);
			return new AuthResponse {
				AccessToken = accessToken,
				UserData = user,
				StatusCode = 200
			};
		}

	}
}

