﻿using System;
using Verifier.Core;

namespace Verifier.Shared
{
	public class TokenData
	{
		public User User;
		public string AccessToken;
		public string Email;
		public string Password;
		public TokenData ()
		{
		}
	}
}

