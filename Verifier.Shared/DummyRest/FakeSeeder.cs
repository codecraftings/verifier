﻿using System;
using Verifier.Core;
using Faker;
using System.Collections.Generic;
using System.Reactive.Linq;
using Akavache;
using System.Threading.Tasks;

namespace Verifier.Shared
{
	public static class FakeSeeder
	{
		public const string KeyTokens = "tokens";
		public const string KeyUsers = "users";
		public const string KeyJobs = "jobs";

		public async static Task Run (ICacheManager cache)
		{
			string last_version = null;
			try {
				last_version = await cache.Get<string> ("fake_seeder_version");
			} catch {
				last_version = "0";
			}
			var id = "1003";
			if (last_version != id) {
				await seed (cache);
				await cache.Put<string>("fake_seeder_version", id);
			}
		}

		private static async Task seed (ICacheManager cache)
		{
			var users = new List<User> ();
			for (var i = 0; i < 15; i++) {
				var user = new User () {
					ID = i + 1,
					Name = NameFaker.MaleName (),
					Email = InternetFaker.Email (),
					Age = NumberFaker.Number (20, 40),
					ProfileTitle = StringFaker.Alpha (15),
					AboutText = TextFaker.Sentences (3),
					BirthDate = DateTimeFaker.BirthDay (20, 40),
					Type = i < 5 ? UserType.Buyer : UserType.Verifier,
					JobCount = NumberFaker.Number (1, 30),
					HireCount = NumberFaker.Number (1, 30),
					TotalSpent = NumberFaker.Number (20, 500),
					OverallSatisfaction = NumberFaker.Number (30, 98),
					Location = new Location {
						Address = LocationFaker.Street (),
						Lat = (36.775567 + NumberFaker.Number (10, 10000) / 100000).ToString (),
						Long = (-119.7600977 + NumberFaker.Number (10, 10000) / 100000).ToString ()
					},
					CreatedAt = DateTimeFaker.DateTimeBetweenMonths (1, 4),
					UpdatedAt = DateTimeFaker.DateTime ()
				};
				users.Add (user);
			}
			var jobs = new List<Job> ();
			for (var i = 0; i < 30; i++) {
				var status = EnumFaker.SelectFrom<JobStatus> ();
				var buyer = users [NumberFaker.Number (0, 4)];
				var applicants = new List<JobApplicant> ();
				for (var j = 0; j < NumberFaker.Number (0, 10); j++) {
					applicants.Add (new JobApplicant {
						Profile = users [NumberFaker.Number (5, 14)],
						Offer = NumberFaker.Number (12, 200),
						CreatedAt = DateTimeFaker.DateTime (),
						UpdatedAt = DateTimeFaker.DateTime ()
					});
				}
				var job = new Job () {
					ID = i + 1,
					Title = TextFaker.Sentence (),
					Buyer = buyer,
					Budget = NumberFaker.Number (12, 100),
					Category = ArrayFaker.SelectFrom<JobCategory> (JobCategories.All ()),
					Description = TextFaker.Sentences (8),
					Status = status,
					Location = new Location {
						Address = LocationFaker.Street (),
						Lat = (36.775567 + NumberFaker.Number (10, 10000) / 100000).ToString (),
						Long = (-119.7600977 + NumberFaker.Number (10, 10000) / 100000).ToString ()
					},
					DateTime = DateTimeFaker.DateTime (),
					Applicants = applicants,
					CreatedAt = DateTimeFaker.DateTime (),
					UpdatedAt = DateTimeFaker.DateTime ()
				};
				job.Report = new JobReport {
					AgreedAmount = NumberFaker.Number (10, 200),
					Buyer = buyer,
					Verifier = applicants.Count>0?applicants [NumberFaker.Number (applicants.Count-1)].Profile:null,
					Job = new Job{
						ID = job.ID
					},
					ReportText = TextFaker.Sentences (15),
					Status = EnumFaker.SelectFrom<ReportStatus> (),
					CreatedAt = DateTimeFaker.DateTime (),
					UpdatedAt = DateTimeFaker.DateTime ()
				};
				jobs.Add (job);
			}
			foreach (var user in users) {
				var reviews = new List<UserReview> ();
				for (var i = 0; i < NumberFaker.Number (15); i++) {
					User writer = null;
					while (writer == null || writer == user)
						writer = users [NumberFaker.Number (14)];
					var job = jobs [NumberFaker.Number (29)];
					reviews.Add (new UserReview {
						Receiver = new User{
							ID = user.ID,
							Name = user.Name
						},
						Writer = new User{
							ID = writer.ID,
							Name = writer.Name
						},
						RelatedJob = new Job{
							ID = job.ID
						},
						ReviewText = TextFaker.Sentences (5),
						SatisfactionScore = NumberFaker.Number (40, 100)
					});
				}
				user.Reviews = reviews;
			}

			var tokens = new List<TokenData> ();
			foreach (var user in users) {
				tokens.Add (new TokenData {
					AccessToken = StringFaker.AlphaNumeric (15),
					Email = "mahfuz" + user.ID + "@gmail.com",
					Password = "bangla",
					User = user
				});
			}

			await cache.Put<List<TokenData>> (KeyTokens, tokens);
			await cache.Put<List<User>> (KeyUsers, users);
			await cache.Put<List<Job>> (KeyJobs, jobs);
		}
	}
}

