﻿using System;
using Ninject;
using Verifier.Core;

namespace Verifier.Shared
{
	public static class App
	{
		public static readonly IIoCContainer IocContainer;

		static App ()
		{
			IocContainer = new Verifier.Shared.IocContainer ();
			StoresRegistry.RegisterStoresToIocContainer (IocContainer);
		}

		public async static void OnAppStartup(){
			await FakeSeeder.Run (App.IocContainer.Get<ICacheManager> ());
			App.IocContainer.Get<IStoreManager> ().StartDispatchLoop ();
		}
		public static void OnAppPause(){

		}
		public static void OnAppResume(){

		}
		public static void OnAppSuspend(){

		}
	}
}

