﻿using System;
using Newtonsoft.Json;
using UIKit;
using CoreGraphics;
namespace Verifier.Shared
{
	public static class Util
	{
		public static TObject clone<TObject> (TObject source)
		{            
			if (Object.ReferenceEquals (source, null)) {
				return default(TObject);
			}
			var deserializeSettings = new JsonSerializerSettings { ObjectCreationHandling = ObjectCreationHandling.Replace };

			return JsonConvert.DeserializeObject<TObject> (JsonConvert.SerializeObject (source), deserializeSettings);
		}

		public static UIImage CircularImage(UIImage image, float w, float h){
			UIGraphics.BeginImageContextWithOptions (new CGSize (w, h), false, 3);
			var context = UIGraphics.GetCurrentContext ();
			context.AddPath (UIBezierPath.FromOval (new CGRect (0, 0, w, h)).CGPath);
			context.Clip ();
			var w1 = image.Size.Width;
			var h1 = image.Size.Height;
			var h2 = h1 / w1 * w;
			if (h2 < h) {
				h2 = h;
			}
			var w2 = w1 / h1 * h2;
			image.Draw (new CGRect (-(w2-w)/2, -(h2-h)/2, w2, h2));
			var img = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();
			return img;
		}
	}
}

