﻿using System;
using Verifier.Core;
using Newtonsoft.Json;
using System.Reactive.Linq;
using Akavache;
using System.Threading.Tasks;
using System.IO;
using Splat;
namespace Verifier.Shared
{
	public class CacheManager:ICacheManager
	{
		public CacheManager ()
		{
			BlobCache.ApplicationName = "Verifier.App";
		}

		#region ICacheManager implementation
		public IObservable<System.Reactive.Unit> Put<T> (string key, T value, bool encrypted = false)
		{
			if (encrypted) {
				return BlobCache.Secure.InsertObject<T> (key, value);
			}
			else {
				return BlobCache.UserAccount.InsertObject<T> (key, value);
			}
		}

		public IObservable<T> Get<T> (string key, bool encrypted = false)
		{
			if(encrypted)
				return BlobCache.Secure.GetObject<T> (key);
			else
				return BlobCache.UserAccount.GetObject<T> (key);
		}


		public async Task<IBitmap> GetImage (string url)
		{
			if (url.StartsWith ("local://")) {
				using(var stream = new FileStream (getPathFromLocalUri (url), FileMode.Open)){
					return await BitmapLoader.Current.Load (stream, null, null);
				}
			}
			return await BlobCache.LocalMachine.LoadImageFromUrl (url);
		}
		#endregion

		string getPathFromLocalUri (string url)
		{
			return url.Replace ("local://", "./");
		}
	}
}

