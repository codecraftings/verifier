﻿using System;

namespace Verifier.Core
{
	public struct Location
	{
		public string Address;
		public string Lat;
		public string Long;
	}
}

