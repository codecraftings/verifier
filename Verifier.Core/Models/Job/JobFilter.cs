﻿using System;
using System.Collections.Generic;

namespace Verifier.Core
{
	public class JobFilter:IEquatable<JobFilter>
	{
		public Location Location;
		public JobFilterSort SortBy;
		public JobCategory[] Categories;
		public JobStatus[] JobStatus;
		public DateTime[] Dates;
		public int[] TimePreference;
		public float Distance;

		#region IEquatable implementation

		public JobFilter(){
			Categories = new JobCategory[]{ };
			JobStatus = new Verifier.Core.JobStatus[]{ };
			Dates = new DateTime[]{ };
			TimePreference = new int[]{ };
			Location = new Location ();
		}
		public bool Equals (JobFilter that)
		{
			if (that == null) {
				return false;
			}
			if (Location.Address != that.Location.Address || Location.Lat != that.Location.Lat || Location.Long != that.Location.Long)
				return false;
			if (SortBy != that.SortBy||Math.Abs(Distance-that.Distance)>0.0001f)
				return false;
			if (Categories.Length != that.Categories.Length || JobStatus.Length != that.JobStatus.Length || Dates.Length != that.Dates.Length || TimePreference.Length != that.TimePreference.Length) {
				return false;
			}
			for (var i = 0; i < Categories.Length; i++) {
				if (Categories [i].Label != that.Categories [i].Label) {
					return false;
				}
			}
			for (var i = 0; i < JobStatus.Length; i++) {
				if (JobStatus [i] == that.JobStatus [i])
					return false;
			}
			for (var i = 0; i < Dates.Length; i++) {
				if (Dates [i] == that.Dates [i])
					return false;
			}
			for (var i = 0; i < TimePreference.Length; i++) {
				if (TimePreference [i] == that.TimePreference [i])
					return false;
			}
			return true;
		}

		public List<Job> Apply(List<Job> jobsList){
			return jobsList;
		}

		#endregion
	}
	public enum JobFilterSort{
		NewestFirst, OldestFirst, MostApplicants, ClosestFirst, MostBudget
	}
}

