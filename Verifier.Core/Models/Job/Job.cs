﻿using System;
using System.Collections.Generic;

namespace Verifier.Core
{
	public class Job
	{
		public long ID;
		public string Title;
		public User Buyer;
		public string Description;
		public Location Location;
		public JobCategory Category;
		public DateTime DateTime;
		public float Budget;
		public JobStatus Status;
		public List<JobApplicant> Applicants;
		public JobReport Report;

		public DateTime CreatedAt;
		public DateTime UpdatedAt;
		public Job ()
		{
		}
	}
}

