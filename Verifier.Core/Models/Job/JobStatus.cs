﻿using System;

namespace Verifier.Core
{
	public enum JobStatus
	{
		Active, ReportPending, ReportSubmitted, ReportReceived, Completed
	}
}

