﻿using System;
using System.Collections.Generic;

namespace Verifier.Core
{
	public class JobReport
	{
		public User Buyer;
		public User Verifier;
		public Job Job;
		public ReportStatus Status;
		public float AgreedAmount;

		public string ReportText;
		public List<ReportAttachment> Attachments;

		public DateTime CreatedAt;
		public DateTime UpdatedAt;
		public JobReport ()
		{
		}
	}
}

