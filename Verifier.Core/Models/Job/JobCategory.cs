﻿using System;

namespace Verifier.Core
{
	public struct JobCategory
	{
		public string Label;
		public JobCategory(string label){
			Label = label;
		}
		public string ThumbIcon{
			get{
				return "local://Icons/"+Label.ToLower().Replace(' ', '_')+".png";
			}
		}
	}
	public static class JobCategories{
		public static JobCategory TwoWheels = new JobCategory("2 Wheels");
		public static JobCategory Car = new JobCategory("Car");
		public static JobCategory Truck = new JobCategory("Truck");
		public static JobCategory Boat = new JobCategory("Boat");
		public static JobCategory Estate = new JobCategory("Estate");
		public static JobCategory Item = new JobCategory("Item");
		public static JobCategory Production = new JobCategory("Production");
		public static JobCategory Other = new JobCategory("Other");

		public static JobCategory[] All(){
			return new JobCategory[] {
				JobCategories.TwoWheels,
				JobCategories.Car,
				JobCategories.Truck,
				JobCategories.Boat,
				JobCategories.Estate,
				JobCategories.Item,
				JobCategories.Production,
				JobCategories.Other
			};
		}
	}
}

