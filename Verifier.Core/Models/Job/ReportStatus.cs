﻿using System;

namespace Verifier.Core
{
	public enum ReportStatus
	{
		Pending, Submitted, Received
	}
}

