﻿using System;

namespace Verifier.Core
{
	public struct ReportAttachment
	{
		public AttachmentType Type;
		public string Link;
		public string Description;
		public string ThumbLink;

		public enum AttachmentType{
			Photo
		}
	}

}

