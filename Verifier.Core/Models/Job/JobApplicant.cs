﻿using System;

namespace Verifier.Core
{
	public class JobApplicant
	{
		public User Profile;
		public float Offer;
		public DateTime CreatedAt;
		public DateTime UpdatedAt;
		public JobApplicant ()
		{
		}
	}
}

