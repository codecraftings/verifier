﻿using System;

namespace Verifier.Core
{
	public class UserReview
	{
		public User Writer;
		public User Receiver;
		public Job RelatedJob;
		public float SatisfactionScore;
		public string ReviewText;
		public UserReview ()
		{
		}
	}
}

