﻿using System;
using System.Collections.Generic;

namespace Verifier.Core
{
	public class User
	{
		public long ID;
		public string Name;
		public string Email;
		public UserType Type;
		public DateTime BirthDate;
		public int Age;
		public float OverallSatisfaction;
		public int JobCount;
		public int HireCount;
		public float TotalSpent;
		public string ProfileTitle;
		public string AboutText;
		public Location Location;
		public List<UserReview> Reviews;
		public DateTime CreatedAt;
		public DateTime UpdatedAt;
		public string ProfilePic{
			get{
				return "local://Sample/sakib.jpg";
			}
		}
		public User ()
		{
		}
	}
}

