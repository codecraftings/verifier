﻿using System;

namespace Verifier.Core
{
	public interface IIoCContainer
	{
		void Bind(Type type1, Type type2, bool singleton = false);
		object Get(Type type);
		T Get<T>();
	}
}

