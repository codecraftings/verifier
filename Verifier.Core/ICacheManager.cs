﻿using System;
using Splat;
using System.Threading.Tasks;

namespace Verifier.Core
{
	public interface ICacheManager
	{
		
		IObservable<System.Reactive.Unit> Put<T> (string key, T value, bool encrypted = false);
		IObservable<T> Get<T>(string key, bool encrypted = false);
		Task<IBitmap> GetImage(string url);
	}
}

