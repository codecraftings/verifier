﻿using System;

namespace Verifier.Core
{
	public interface IViewController
	{
		NavigationStore.Screen ScreenIdentifier{get;}
		bool IsViewVisible{get;}
		bool IsViewLoaded{get;}
		IIoCContainer IocContainer{get;}
		void RenderState();
	}
}

