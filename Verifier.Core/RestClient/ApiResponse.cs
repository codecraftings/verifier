﻿using System;

namespace Verifier.Core
{
	public class ApiResponse
	{
		public int StatusCode{ get; set; }

		public ApiResponse ()
		{
		}
	}

	public class ApiResponse<T>:ApiResponse
	{
		public T Data{ get; set; }
	}
}

