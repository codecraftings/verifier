﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Verifier.Core
{
	public class DummyRestRequest:IRestRequest
	{
		public String ResourcePath{ get; set; }

		public String HttpMethod{ get; set; }
		public string ContentType{get;set;}

		public List<Tuple<string, object>> Headers{ get; set; }

		public List<Tuple<string, object>> UrlSegments{ get; set; }

		public List<Tuple<string, object>> Parameters{ get; set; }

		public List<Tuple<string, Stream, string>> FileParameters{ get; set; }

		public List<Tuple<string, object>> QueryParams{ get; set; }

		public DummyRestRequest (string resourcePath, string httpMethod)
		{
			ResourcePath = resourcePath;
			HttpMethod = httpMethod;
			Headers = new List<Tuple<string, object>> ();
			UrlSegments = new List<Tuple<string, object>> ();
			Parameters = new List<Tuple<string, object>> ();
			FileParameters = new List<Tuple<string, Stream, string>> ();
			QueryParams = new List<Tuple<string, object>> ();
		}

		#region IRestRequest implementation

		public void SetContentType (string contentType)
		{
			ContentType = contentType;
		}

		public void AddHeader (string key, object value)
		{
			Headers.Add (Tuple.Create<string, object> (key, value));
		}

		public void AddUrlSegment (string key, object value)
		{
			UrlSegments.Add (Tuple.Create<string, object> (key, value));
		}

		public void AddParameter (string key, object value)
		{
			Parameters.Add (Tuple.Create<string, object> (key, value));
		}
		public T getParam<T>(string key){
			foreach (var param in Parameters) {
				if (param.Item1 == key)
					return (T)param.Item2;
			}
			foreach (var param in QueryParams) {
				if (param.Item1 == key)
					return (T)param.Item2;
			}
			foreach (var param in UrlSegments) {
				if (param.Item1 == key)
					return (T)param.Item2;
			}
			return default(T);
		}
		public void AddFileParameter (string key, System.IO.Stream fileStream, string fileName)
		{
			FileParameters.Add (Tuple.Create<string, Stream, string> (key, fileStream, fileName));
		}

		public void AddQueryString (string key, string value)
		{
			QueryParams.Add (Tuple.Create<string, object> (key, value));
		}

		#endregion
	}
}

