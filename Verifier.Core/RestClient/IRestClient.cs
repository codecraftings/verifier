﻿using System;
using System.Threading.Tasks;

namespace Verifier.Core
{
	public interface IRestClient
	{
		IRestRequest CreateRequest(string apiPath, string httpMethod);
		T Execute<T>(IRestRequest request);
		Task<T> ExecuteAsync<T>(IRestRequest request);
	}
}

