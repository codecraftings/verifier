﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Verifier.Core
{
	public interface IRestRequest
	{
		void SetContentType(string contentType);

		void AddHeader (string key, object value);

		void AddUrlSegment(string key, object value);

		void AddParameter(string key, object value);

		void AddFileParameter(string key, Stream fileStream, string fileName);

		void AddQueryString(string key, string value);
	}
}

