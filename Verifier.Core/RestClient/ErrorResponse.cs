﻿using System;

namespace Verifier.Core
{
	public class ErrorResponse:ApiResponse
	{
		public ApiError Error{ get; set;}

		public ErrorResponse ()
		{
		}

		public class ApiError{
			public int Code;
			public int Message;
		}
	}
}

