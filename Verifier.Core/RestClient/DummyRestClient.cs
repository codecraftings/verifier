﻿using System;
using System.Threading.Tasks;

namespace Verifier.Core
{
	public class DummyRestClient:IRestClient
	{
		public DummyRestClient ()
		{
		}

		#region IRestClient implementation

		public IRestRequest CreateRequest (string apiPath, string httpMethod)
		{
			return new DummyRestRequest (apiPath, httpMethod);
		}

		public T Execute<T> (IRestRequest request)
		{
			var req = request as DummyRestRequest;
			if (req == null)
				throw new ArgumentNullException ();
			return (T)getDummyResponse (req);
		}

		public Task<T> ExecuteAsync<T> (IRestRequest request)
		{
			return Task.Run<T> (async () => {
				await Task.Delay(1000);
				return Execute<T> (request);
			});
		}

		#endregion

		private object getDummyResponse(DummyRestRequest request){
			object response = null;
			switch (request.ResourcePath) {
				case "/login":
					
				break;
			}

			return response;
		}
	}
}

