﻿using System;

namespace Verifier.Core
{
	public class AuthResponse:ApiResponse
	{
		public string AccessToken{get;set;}
		public User UserData{ get; set;}
		public AuthResponse ()
		{
		}
	}
}

