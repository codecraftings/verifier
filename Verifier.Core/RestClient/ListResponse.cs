﻿using System;
using System.Collections.Generic;

namespace Verifier.Core
{
	public class ListResponse<T>:ApiResponse
	{
		public long TotalItemCount{ get; set; }

		public int CurrentItemCount{ get; set; }

		public List<T> Items{ get; set; }

		public ListResponse ()
		{
		}
	}
}

