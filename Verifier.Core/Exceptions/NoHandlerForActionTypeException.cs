﻿using System;

namespace Verifier.Core
{
	public class NoHandlerForActionTypeException:Exception
	{
		public NoHandlerForActionTypeException (string msg=""):base(msg)
		{
		}
	}
}

