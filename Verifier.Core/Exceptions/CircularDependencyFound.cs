﻿using System;

namespace Verifier.Core
{
	public class CircularDependencyFound:Exception
	{
		public CircularDependencyFound (string msg) : base (msg)
		{
		}
	}
}

