﻿using System;

namespace Verifier.Core
{
	public class RedundantActionHandlerException:Exception
	{
		public RedundantActionHandlerException (string msg):base(msg)
		{
		}
	}
}

