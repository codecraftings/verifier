﻿using System;

namespace Verifier.Core
{
	public class DependencyNotFound:Exception
	{
		public DependencyNotFound (string msg):base(msg)
		{
		}
	}
}

