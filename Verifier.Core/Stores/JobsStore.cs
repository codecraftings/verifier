﻿using System;
using System.Collections.Generic;

namespace Verifier.Core
{
	public class JobsStoreState:IStoreState
	{
		public JobsStore.Status Status;
		public string LastError;
		public List<Job> UserPostedJobs;
		public List<Job> UserPostedJobsFiltered;
		public JobFilter UserPostedJobsFilter;
		public List<Job> BuyerActiveJobs;
		public List<Job> BuyerActiveJobsFiltered;
		public JobFilter BuyerActiveJobsFilter;

		public Job LoadedJob;
		public List<Job> LoadedJobCache;

		public JobsStoreState ()
		{
			Status = JobsStore.Status.Active;
			UserPostedJobs = new List<Job> ();
			UserPostedJobsFiltered = new List<Job> ();
			UserPostedJobsFilter = new JobFilter ();
			BuyerActiveJobs = new List<Job> ();
			BuyerActiveJobsFiltered = new List<Job> ();
			BuyerActiveJobsFilter = new JobFilter ();
			LoadedJobCache = new List<Job>();
		}
	}

	public class JobsStore:AStore<JobsStoreState>
	{
		public JobsStore (ICacheManager cache, IStoreManager store) : base (cache, store)
		{
			RegisterActionHandler<FetchJobsAction> (handleFetchJobsRequested);
			RegisterActionHandler<FetchJobsCompleteAction> (handleFetchJobComplete);
			RegisterActionHandler<LoadJobAction> (handleLoadJobRequest);
			RegisterActionHandler<LoadJobCompleteAction> (handlLoadJobComplete);
			RegisterActionHandler<LoadErrorAction>(handleLoadError);
		}

		void handleLoadError (LoadErrorAction action, JobsStoreState state)
		{
			state.Status = Status.Active;
			state.LastError = action.Message;
		}



		void handleLoadJobRequest (LoadJobAction action, JobsStoreState state)
		{
			state.Status = Status.LoadingJob;
			state.LastError = "";
			var cached = state.LoadedJobCache.Find ((Job obj) => obj.ID == action.Job.ID);
			if (cached != null) {
				state.LoadedJob = cached;
			} else {
				state.LoadedJob = action.Job;
			}
		}

		void handlLoadJobComplete (LoadJobCompleteAction action, JobsStoreState state)
		{
			state.Status = Status.Active;
			state.LoadedJob = action.Job;
			state.LoadedJobCache.RemoveAll ((Job obj) => obj.ID == action.Job.ID);
			state.LoadedJobCache.Add (action.Job);
			if (state.LoadedJobCache.Count > 20) {
				state.LoadedJobCache.RemoveAt (0);
			}
		}

		void handleFetchJobsRequested (FetchJobsAction action, JobsStoreState state)
		{
			state.Status = Status.FetchingList;
			state.LastError = "";
			switch (action.ListType) {
			case ListType.BuyerActiveJobs:
				state.BuyerActiveJobsFilter = action.Filter;
				state.BuyerActiveJobsFiltered = ApplyJobFilter (state.BuyerActiveJobs, state.BuyerActiveJobsFilter);
				break;
			case ListType.PostedJobs:
				state.UserPostedJobsFilter = action.Filter;
				state.UserPostedJobsFiltered = ApplyJobFilter (state.UserPostedJobs, state.UserPostedJobsFilter);
				break;
			case ListType.FindJobs:
				//todo
				break;
			case ListType.AppliedJobs:
				//todo
				break;
			}
		}

		void handleFetchJobComplete (FetchJobsCompleteAction action, JobsStoreState state)
		{
			switch (action.ListType) {
			case ListType.BuyerActiveJobs:
				if (!action.Filter.Equals(state.BuyerActiveJobsFilter))
					return;
				if (action.Append)
					state.BuyerActiveJobs = appendJobs (state.BuyerActiveJobs, action.Jobs);
				else
					state.BuyerActiveJobs = action.Jobs;
				state.BuyerActiveJobsFiltered = state.BuyerActiveJobs;
				break;
			case ListType.PostedJobs:
				if (!action.Filter.Equals(state.UserPostedJobsFilter))
					return;
				if (action.Append)
					state.UserPostedJobs = appendJobs (state.UserPostedJobs, action.Jobs);
				else
					state.UserPostedJobs = action.Jobs;
				state.UserPostedJobsFiltered = state.UserPostedJobs;
				break;
			case ListType.FindJobs:
				//todo
				break;
			case ListType.AppliedJobs:
				//todo
				break;
			}
			state.Status = Status.Active;
		}
		List<Job> appendJobs(List<Job> oldList, List<Job> newList){
			oldList.RemoveAll (newList.Contains);
			oldList.AddRange (newList);
			return oldList;
		}
		public List<Job> ApplyJobFilter(List<Job> allJobs, JobFilter filter){
			return filter.Apply (allJobs);
		}
		#region implemented abstract members of AStore

		protected override JobsStoreState getInitialStateObject ()
		{
			return new JobsStoreState ();
		}

		#endregion

		public class FetchJobsAction:IAction
		{
			public ListType ListType;

			public JobFilter Filter{ get; set; }

			public FetchJobsAction (ListType type, JobFilter filter)
			{
				ListType = type;
				Filter = filter;
			}
		}

		public class FetchJobsCompleteAction:IAction
		{
			public ListType ListType;

			public JobFilter Filter;

			public List<Job> Jobs;
			public bool Append;

			public FetchJobsCompleteAction (ListType type, List<Job> jobs, JobFilter filter, bool append = true)
			{
				ListType = type;
				Jobs = jobs;
				Filter = filter;
				Append = append;
			}
		}

		public class LoadJobAction:IAction
		{
			public Job Job;

			public LoadJobAction (Job job)
			{
				Job = job;
			}
		}

		public class LoadJobCompleteAction:IAction
		{
			public Job Job;

			public LoadJobCompleteAction (Job job)
			{
				Job = job;
			}
		}
		public class LoadErrorAction:IAction{
			public string Message;
			public LoadErrorAction(string msg){
				Message = msg;
			}
		}
		public enum ListType
		{
			PostedJobs,
			BuyerActiveJobs,
			FindJobs,
			AppliedJobs
		}

		public enum Status
		{
			Waiting,
			FetchingList,
			LoadingJob,
			Active
		}
	}
}

