﻿using System;

namespace Verifier.Core
{
	public class UsersStoreState:IStoreState{

	}
	public class UsersStore:AStore<UsersStoreState>
	{
		public UsersStore (ICacheManager cache, IStoreManager store):base(cache, store)
		{
		}

		#region implemented abstract members of AStore

		protected override UsersStoreState getInitialStateObject ()
		{
			return new UsersStoreState ();
		}

		#endregion
	}
}

