﻿using System;

namespace Verifier.Core
{
	public class RegistrationStoreState:IStoreState{
		public UserType IntendedUserType{get;set;}
		public string Status{get;set;}
		public string ErrorMessage;
		public RegistrationStoreState(){
			Status = RegistrationStore.StatusIdle;
		}
	}
	public class RegistrationStore:AStore<RegistrationStoreState>
	{
		public const string StatusWaiting = "waiting";
		public const string StatusIdle = "idle";
		#region implemented abstract members of AStore
		protected override RegistrationStoreState getInitialStateObject ()
		{
			return new RegistrationStoreState ();
		}

		#endregion

		public RegistrationStore (ICacheManager cache, IStoreManager store):base(cache, store)
		{
			RegisterActionHandler<SetUserTypeAction>(HandleSetUserType);
			RegisterActionHandler<RegisterErrorAction>(handleRegistrationError);
			RegisterActionHandler<RegistrationRequested>(handleRegistrationRequested);
			RegisterActionHandler<RegistrationSuccessAction> (handleRegistrationSuccess);
		}

		void handleRegistrationSuccess (RegistrationSuccessAction action, RegistrationStoreState state)
		{
			state.ErrorMessage = "";
			state.Status = StatusIdle;
		}

		void handleRegistrationRequested (RegistrationRequested action, RegistrationStoreState state)
		{
			state.ErrorMessage = "";
			state.Status = StatusWaiting;
		}

		void handleRegistrationError (RegisterErrorAction action, RegistrationStoreState state)
		{
			state.ErrorMessage = action.Message;
			state.Status = StatusIdle;
		}

		void HandleSetUserType (SetUserTypeAction action, RegistrationStoreState state)
		{
			state.IntendedUserType = action.UserType;
		}
		public class RegistrationSuccessAction:IAction{

		}
		public class RegistrationRequested:IAction{
		}
		public class RegisterErrorAction:IAction{
			public string Message;
			public RegisterErrorAction(string msg){
				Message = msg;
			}
		}
		public class SetUserTypeAction:IAction{
			public UserType UserType;
			public SetUserTypeAction(UserType userType){
				UserType = userType;
			}
		}
	}
}

