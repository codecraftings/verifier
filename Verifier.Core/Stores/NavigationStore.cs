﻿using System;

namespace Verifier.Core
{
	public class NavigationStoreState:IStoreState{
		public NavigationStore.Screen CurrentScreen{get;set;}
	}
	public class NavigationStore:AStore<NavigationStoreState>
	{
		SessionStore sessionStore;

		JobsStore jobsStore;

		public NavigationStore (ICacheManager cache, IStoreManager store, SessionStore sessionStore, JobsStore jobsStore):base(cache, store)
		{
			this.jobsStore = jobsStore;
			this.sessionStore = sessionStore;
			RegisterActionHandler<NavigationAction>(handleNavigateAction);
			RegisterActionHandler<SessionStore.UserLoggedIn>(handleUserLoggedIn, new IStore[]{sessionStore});
			RegisterActionHandler<SessionStore.UserLoggedOut>(handleUserLoggedOut, new IStore[]{sessionStore});
			RegisterActionHandler<SessionStore.SessionRestoredAction> (handleUserLoggedIn, new IStore[]{sessionStore});
			RegisterActionHandler<JobsStore.LoadJobAction>(handleLoadJobAction, new IStore[]{jobsStore});
		}

		void handleLoadJobAction (JobsStore.LoadJobAction action, NavigationStoreState state)
		{
			state.CurrentScreen = Screens.JobViewScreen;
		}

		void handleUserLoggedOut (SessionStore.UserLoggedOut action, NavigationStoreState state)
		{
			state.CurrentScreen = Screens.OnBoardScreen;
		}

		void handleUserLoggedIn (IAction action, NavigationStoreState state)
		{
			if (sessionStore.State.UserData.Type == UserType.Buyer)
				state.CurrentScreen = Screens.BuyerHome;
			else
				state.CurrentScreen = Screens.VerifierHome;
		}

		void handleNavigateAction (NavigationAction action, NavigationStoreState state)
		{
			state.CurrentScreen = action.ToScreen;
		}

		protected override NavigationStoreState getInitialStateObject ()
		{
			var state = new NavigationStoreState ();
			state.CurrentScreen = Screens.OnBoardScreen;
			return state;
		}

		public static class Screens{
			public static Screen OnBoardScreen = new Screen("OnBoardingScreen");
			public static Screen BuyerHome = new Screen("idBuyerHome");
			public static Screen VerifierHome = new Screen("idHomeScreen");
			public static Screen LoginScreen = new Screen("idLoginScreen");
			public static Screen RegisterScreen = new Screen("idRegisterScreen");
			public static Screen JobViewScreen = new Screen("idJobViewScreen");
		}
		public struct Screen{
			public string StoryBoardId{get;set;}
			public Screen(string storyboardId){
				StoryBoardId = storyboardId;
			}
		}
		public class NavigationAction:IAction{
			public Screen ToScreen{get;set;}
			public NavigationAction(Screen toScreen){
				ToScreen = toScreen;
			}
		}
	}
}

