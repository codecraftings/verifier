﻿using System;

namespace Verifier.Core
{
	public class RehydrateStoreAction:IAction
	{
		public Type StoreType;

		public RehydrateStoreAction (Type storeType)
		{
			this.StoreType = storeType;
		}
	}
}

