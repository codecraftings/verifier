﻿using System;

namespace Verifier.Core
{
	public class DehydrateStoreAction:IAction
	{
		public Type StoreType;

		public DehydrateStoreAction (Type storeType)
		{
			this.StoreType = storeType;
		}
	}
}

