﻿using System;

namespace Verifier.Core
{
	public class SessionStoreState:IStoreState
	{
		public User UserData{ get; set; }

		public string AccessToken{ get; set; }

		public string Status{ get; set; }
		public string ErrorMessage{ get; set; }
	}

	public class SessionStore:AStore<SessionStoreState>
	{
		public const string UpdateLoginError = "login_error";
		public const string UpdateLogoutError = "logout_error";

		public const string StatusLoggedIn = "logged_in";
		public const string StatusLoggedOut = "logged_out";
		public const string StatusWaiting = "waiting";

		public SessionStore (ICacheManager cacheManager, IStoreManager storeManager) : base (cacheManager, storeManager)
		{
			RegisterActionHandler<UserLoggedIn> (handleUserLoggedIn);
			RegisterActionHandler<UserLoggedOut> (handleUserLoggedOut);
			RegisterActionHandler<UserLogInRequested> (handleLoginRequested);
			RegisterActionHandler<UserLogOutRequested> (handleLogOutRequested);
			RegisterActionHandler<UserLoginError> (handleLoginError);
			RegisterActionHandler<UserLogoutError> (handleLogoutError);
			RegisterActionHandler<SessionRestoredAction>(handleSessionRestored);
		}

		void handleSessionRestored (SessionRestoredAction arg1, SessionStoreState arg2)
		{
					
		}

		protected override SessionStoreState getInitialStateObject ()
		{
			var state = new SessionStoreState ();
			state.Status = StatusWaiting;
			return state;
		}
		protected override void AfterRehydration (SessionStoreState state)
		{
			base.AfterRehydration (state);
			if (state.Status == StatusWaiting)
				state.Status = StatusLoggedOut;
		}
		void handleLogoutError (UserLogoutError obj, SessionStoreState state)
		{
			state.ErrorMessage = obj.Message;
			if (state.AccessToken != null)
				state.Status = StatusLoggedIn;
			else
				state.Status = StatusLoggedOut;
		}

		void handleLoginError (UserLoginError obj, SessionStoreState state)
		{
			state.ErrorMessage = obj.Message;
			state.Status = StatusLoggedOut;
		}

		void handleLogOutRequested (UserLogOutRequested obj, SessionStoreState state)
		{
			state.Status = StatusWaiting;
			state.ErrorMessage = "";
		}

		void handleLoginRequested (UserLogInRequested obj, SessionStoreState state)
		{
			state.Status = StatusWaiting;
			state.ErrorMessage = "";
		}

		void handleUserLoggedIn (UserLoggedIn obj, SessionStoreState state)
		{
			state.UserData = obj.UserData;
			state.AccessToken = obj.AccessToken;
			state.Status = StatusLoggedIn;
		}

		void handleUserLoggedOut (UserLoggedOut obj, SessionStoreState state)
		{
			state.UserData = null;
			state.AccessToken = null;
			state.Status = StatusLoggedOut;
		}
		public class SessionRestoredAction:IAction{

		}
		public class UserLogInRequested:IAction
		{

		}

		public class UserLogOutRequested:IAction
		{

		}

		public class UserLoggedIn:IAction
		{
			public User UserData;

			public string AccessToken;

			public UserLoggedIn (User userData, string accessToken)
			{
				this.AccessToken = accessToken;
				this.UserData = userData;
			}
		}

		public class UserLoggedOut:IAction
		{
			
		}

		public class UserLoginError:IAction
		{
			
			public string Message;

			public UserLoginError (string msg)
			{
				this.Message = msg;

			}
		}

		public class UserLogoutError:IAction
		{

			public string Message;

			public UserLogoutError (string msg)
			{
				this.Message = msg;

			}
		}
	}
}

