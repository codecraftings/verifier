﻿using System;

namespace Verifier.Core
{
	public class NavigationActionCreators
	{
		IStoreManager storeManager;

		public NavigationActionCreators (IStoreManager storeManager)
		{
			this.storeManager = storeManager;
		}

		public void Navigate(NavigationStore.Screen toScreen){
			storeManager.DispatchAction<NavigationStore.NavigationAction>(new NavigationStore.NavigationAction(toScreen));
		}

		public void NavigateToLogin(UserType userType){
			storeManager.DispatchAction<RegistrationStore.SetUserTypeAction>(new RegistrationStore.SetUserTypeAction(userType));
			Navigate(NavigationStore.Screens.LoginScreen);
		}
	}
}

