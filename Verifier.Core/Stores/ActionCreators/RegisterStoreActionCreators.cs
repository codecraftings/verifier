﻿using System;

namespace Verifier.Core
{
	public class RegisterStoreActionCreators
	{
		IStoreManager storeManager;

		IRestClient restClient;

		public RegisterStoreActionCreators (IStoreManager storeManger, IRestClient restClient)
		{
			this.restClient = restClient;
			this.storeManager = storeManger;
		}

		public async void TryRegister(string name, UserType userType, string email, string password, string confirm_password){
			storeManager.DispatchAction<RegistrationStore.RegistrationRequested>(new RegistrationStore.RegistrationRequested());
			var validity = validateFileds(name, email, password, confirm_password);
			if (validity!=null) {
				storeManager.DispatchAction<RegistrationStore.RegisterErrorAction> (new RegistrationStore.RegisterErrorAction (validity));
			} else {
				var request = restClient.CreateRequest ("/signup", "post");
				request.AddParameter ("name", name);
				request.AddParameter ("user_type", userType);
				request.AddParameter ("email", email);
				request.AddParameter ("password", password);
				request.AddParameter ("password_confirmation", confirm_password);
				try{
					AuthResponse response = await restClient.ExecuteAsync<AuthResponse>(request);
					storeManager.DispatchAction<SessionStore.UserLoggedIn> (new SessionStore.UserLoggedIn (response.UserData, response.AccessToken));
					//storeManager.DispatchAction<NavigationStore.NavigationAction>(new NavigationStore.NavigationAction(NavigationStore.Screens.HomeScreen));
					storeManager.DehydrateStore<SessionStore>();
					storeManager.DispatchAction<RegistrationStore.RegistrationSuccessAction>(new RegistrationStore.RegistrationSuccessAction());
				}catch(Exception e){
					storeManager.DispatchAction<RegistrationStore.RegisterErrorAction> (new RegistrationStore.RegisterErrorAction (e.Message));
				}
			}
		}
		private string validateFileds(string name, string email, string password, string confirm_password){
			if (String.IsNullOrWhiteSpace (name)) {
				return "Full Name is required";
			} else if (String.IsNullOrWhiteSpace (email)) {
				return "Valid email address required";
			} else if (String.IsNullOrWhiteSpace (password)) {
				return "Password is required";
			}else if (password != confirm_password) {
				return "Password must match with confirm password";
			} else {
				return null;
			}
		}
	}
}

