﻿using System;

namespace Verifier.Core
{
	public class SessionActionsCreator
	{
		readonly IRestClient restClient;

		readonly IStoreManager storeManager;

		public SessionActionsCreator (IRestClient restClient, IStoreManager storeManager)
		{
			this.storeManager = storeManager;
			this.restClient = restClient;
		}
		public void RestoreSession(){
			storeManager.DispatchAction<SessionStore.SessionRestoredAction>(new SessionStore.SessionRestoredAction());
		}
		public async void TryLogin (string email, string password)
		{
			storeManager.DispatchAction<SessionStore.UserLogInRequested> (new SessionStore.UserLogInRequested ());
			var request = restClient.CreateRequest ("/login", "post");
			request.AddParameter ("email", email);
			request.AddParameter ("password", password);
			AuthResponse response;
			try {
				response = await restClient.ExecuteAsync<AuthResponse> (request);
				storeManager.DispatchAction<SessionStore.UserLoggedIn> (new SessionStore.UserLoggedIn (response.UserData, response.AccessToken));
				storeManager.DehydrateStore<SessionStore>();
			} catch (Exception e) {
				storeManager.DispatchAction<SessionStore.UserLoginError> (new SessionStore.UserLoginError (e.Message));
			}
		}

		public void TryLogout ()
		{
			storeManager.DispatchAction<SessionStore.UserLogOutRequested> (new SessionStore.UserLogOutRequested ());
			var request = restClient.CreateRequest ("/logout", "post");
			storeManager.DispatchAction<SessionStore.UserLoggedOut> (new SessionStore.UserLoggedOut ());
			storeManager.DehydrateStore<SessionStore> ();
			restClient.ExecuteAsync<SuccessResponse> (request);
		}
	}
}

