﻿using System;
using System.Collections.Generic;

namespace Verifier.Core
{
	public class JobsActionCreators
	{
		IStoreManager storeManager;

		IRestClient restClient;

		public JobsActionCreators (IStoreManager storeManager, IRestClient restClient)
		{
			this.restClient = restClient;
			this.storeManager = storeManager;
		}

		public async void LoadJob(Job job){
			storeManager.DispatchAction<JobsStore.LoadJobAction>(new JobsStore.LoadJobAction(job));
			var request = restClient.CreateRequest ("/jobs/{id}", "get");
			request.AddUrlSegment ("id", job.ID.ToString());
			try{
				var response = await restClient.ExecuteAsync<Job>(request);
				storeManager.DispatchAction<JobsStore.LoadJobCompleteAction>(new JobsStore.LoadJobCompleteAction(response));
			}catch(Exception e){
				storeManager.DispatchAction<JobsStore.LoadErrorAction> (new JobsStore.LoadErrorAction (e.Message));
			}
		}
		public async void FetchPostedJobs(User user, JobFilter filter, int offset = 0){
			storeManager.DispatchAction<JobsStore.FetchJobsAction>(new JobsStore.FetchJobsAction(JobsStore.ListType.PostedJobs, filter));
			var request = restClient.CreateRequest ("/users/{id}/jobs", "post");
			request.AddUrlSegment ("id", user.ID);
			request.AddParameter ("filter", filter);
			request.AddParameter ("offset", offset);
			try{
				var response = await restClient.ExecuteAsync<List<Job>>(request);
				storeManager.DispatchAction<JobsStore.FetchJobsCompleteAction>(
					new JobsStore.FetchJobsCompleteAction(JobsStore.ListType.PostedJobs, response, filter, offset>0));
			}catch(Exception e){
				storeManager.DispatchAction<JobsStore.LoadErrorAction> (new JobsStore.LoadErrorAction (e.Message));
			}
		}
		public async void FetchBuyerActiveJobs(User user, JobFilter filter, int offset = 0){
			storeManager.DispatchAction<JobsStore.FetchJobsAction>(new JobsStore.FetchJobsAction(JobsStore.ListType.BuyerActiveJobs, filter));
			var request = restClient.CreateRequest ("/users/{id}/jobs", "post");
			request.AddUrlSegment ("id", user.ID);
			request.AddParameter ("filter", filter);
			request.AddParameter ("offset", offset);
			try{
				var response = await restClient.ExecuteAsync<List<Job>>(request);
				storeManager.DispatchAction<JobsStore.FetchJobsCompleteAction>(
					new JobsStore.FetchJobsCompleteAction(JobsStore.ListType.BuyerActiveJobs, response, filter, offset>0));
			}catch(Exception e){
				storeManager.DispatchAction<JobsStore.LoadErrorAction> (new JobsStore.LoadErrorAction (e.Message));
			}
		}
	}
}

