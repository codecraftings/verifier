﻿using System;

namespace Verifier.Core
{
	public class MessagesStoreState:IStoreState{
	}
	public class MessagesStore:AStore<MessagesStoreState>
	{
		public MessagesStore (ICacheManager cache, IStoreManager storeManager):base(cache, storeManager)
		{
		}

		#region implemented abstract members of AStore

		protected override MessagesStoreState getInitialStateObject ()
		{
			return new MessagesStoreState ();
		}

		#endregion
	}
}

