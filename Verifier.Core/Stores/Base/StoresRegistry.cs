﻿using System;

namespace Verifier.Core
{
	public static class StoresRegistry
	{
		public static Type[] GetStoreClasses(){
			return new Type[] {
				typeof(NavigationStore),
				typeof(SessionStore),
				typeof(RegistrationStore),
				typeof(JobsStore),
				typeof(MessagesStore),
				typeof(UsersStore)
			};
		}
		public static void RegisterStoresToIocContainer(IIoCContainer container){
			var stores = GetStoreClasses();
			foreach (var store in stores) {
				container.Bind (store, null, true);
			}
			//just instantiating so that all actions gets registers on boot
			foreach (var store in stores) {
				container.Get (store);
			}
		}
	}
}

