﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Reactive.Linq;

namespace Verifier.Core
{
	public abstract class AStore<T>:IStore where T:class, IStoreState
	{

		private T _state;

		public T State {
			get { 
				if (_state == default(T))
					_state = getInitialStateObject ();
				return _state; 
			}
		}
		protected virtual bool ShouldEncrypted{
			get{
				return false;
			}
		}

		[JsonIgnore]
		public ICacheManager CacheManager{ get; private set; }

		[JsonIgnore]
		public IStoreManager StoreManager{ get; private set; }

		public bool IsRehydrated = false;
		public const string UpdateRehydrationComplete = "rehydration_complete";

		protected AStore (ICacheManager cacheManager, IStoreManager storeManager)
		{
			CacheManager = cacheManager;
			StoreManager = storeManager;
			RegisterActionHandler<DehydrateStoreAction> (this.HandleDehydrateRequest);
			RegisterActionHandler<RehydrateStoreAction> (this.HandleRehydrateRequest);
		}

		protected abstract T getInitialStateObject ();

		protected void RegisterActionHandler<TAction> (Action<TAction, T> handlerFn) where TAction:IAction{
			RegisterActionHandler<TAction> (handlerFn, new IStore[]{ });
		}
		protected void RegisterActionHandler<TAction> (Action<TAction, T> handlerFn, IStore[] dependencies) where TAction:IAction
		{
			StoreManager.RegisterActionHandler<TAction> (this, dependencies, action => {
				//var state = clone<T> (State);
				handlerFn.Invoke (action, State);
				//_state = state;
				BroadcastUpdate ();
			});
		}

		private static TObject clone<TObject> (TObject source)
		{            
			if (Object.ReferenceEquals (source, null)) {
				return default(TObject);
			}
			var deserializeSettings = new JsonSerializerSettings { ObjectCreationHandling = ObjectCreationHandling.Replace };

			return JsonConvert.DeserializeObject<TObject> (JsonConvert.SerializeObject (source), deserializeSettings);
		}

		protected virtual void HandleDehydrateRequest (DehydrateStoreAction action, T state)
		{
			if (action.StoreType == null || action.StoreType == this.GetType ()) {
				CacheManager.Put<string> (this.GetType ().FullName, JsonConvert.SerializeObject (state), ShouldEncrypted);
			}
		}

		protected async virtual void HandleRehydrateRequest (RehydrateStoreAction action, T state)
		{
			if (action.StoreType == null || action.StoreType == this.GetType ()) {
				if (IsRehydrated)
					return;
				string json;
				try {
					json = await CacheManager.Get<string> (this.GetType ().FullName, ShouldEncrypted);
				} catch {
					AfterRehydration (state);
					return;
				}
				json = BeforeRehydration (json);
				if (!String.IsNullOrWhiteSpace (json))
					JsonConvert.PopulateObject (json, state);
				AfterRehydration (state);
				//temporary solution
			}
		}

		protected virtual string BeforeRehydration (string json)
		{
			return json;
		}

		protected virtual void AfterRehydration (T state)
		{
			IsRehydrated = true;
			BroadcastUpdate ();
		}

		public virtual void ListenForUpdate (Action<StoreUpdated> listenerFn)
		{
			StoreManager.RegisterEventListener<StoreUpdated> (this, listenerFn);
		}

		protected virtual void BroadcastUpdate ()
		{
			StoreManager.DispatchEvent<StoreUpdated> (this, new StoreUpdated ());
		}
	}
}

