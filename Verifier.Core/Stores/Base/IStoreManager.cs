﻿using System;

namespace Verifier.Core
{
	public interface IStoreManager
	{
		void RegisterActionHandler<T> (IStore storeObj, Action<T> handlerFn) where T:IAction;
		void RegisterActionHandler<T> (IStore storeObj, IStore[] dependencies, Action<T> handlerFn) where T:IAction;
		void RegisterEventListener<T> (IStore storeObj, Action<T> listener) where T:IEvent;
		void UnregisterEventListener<T> (IStore storeObj, Action<T> listener) where T:IEvent;
		void DispatchAction<T> (T action) where T:IAction;
		void DispatchEvent<T> (IStore storeObj, T eventObj) where T:IEvent;

		T Bind<T>(IViewController viewController) where T:IStore;
		void DehydrateStore<T>() where T:IStore;

		void StartDispatchLoop ();
		void StopDispatchLoop ();
	}
}

