﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Verifier.Core
{
	public class StoreManager:IStoreManager
	{
		private readonly Dictionary<Type, List<ActionHandler>> actionHandlersMap;
		private readonly Dictionary<IStore, Dictionary<Type, List<Tuple<object,Action<IEvent>>>>> eventHandlers;
		private readonly Queue<Tuple<Type, IAction>> actionsQueue;
		private readonly Queue<Tuple<IStore, Type, IEvent>> eventsQueue;

		private static readonly object _queueLock = new object ();
		private static readonly object _actionHandlersLock = new object ();
		private static readonly object _eventHandlersLock = new object ();

		private readonly ManualResetEventSlim mEvent;

		readonly IThreadManager threadManager;
		CancellationTokenSource cts;
		bool dispatchLoopRuning = false;
		object dispatchThreadObject;

		public StoreManager (IThreadManager threadManager)
		{
			this.threadManager = threadManager;
			actionHandlersMap = new Dictionary<Type, List<ActionHandler>> ();
			eventHandlers = new Dictionary<IStore, Dictionary<Type, List<Tuple<object, Action<IEvent>>>>> ();
			actionsQueue = new Queue<Tuple<Type, IAction>> ();
			eventsQueue = new Queue<Tuple<IStore, Type, IEvent>> ();
			mEvent = new ManualResetEventSlim ();
		}

		public T Bind<T> (IViewController viewController) where T:IStore
		{
			var store = viewController.IocContainer.Get<T> ();
			DispatchAction<RehydrateStoreAction> (new RehydrateStoreAction (store.GetType ()));
			store.ListenForUpdate (e => {
				if (viewController.IsViewLoaded && viewController.IsViewVisible)
					viewController.RenderState ();
			});
			return store;
		}

		public void DehydrateStore<T> () where T : IStore
		{
			DispatchAction<DehydrateStoreAction> (new DehydrateStoreAction (typeof(T)));
		}

		public object GetDispatchThread ()
		{
			return dispatchThreadObject;
		}

		public void StartDispatchLoop ()
		{
			cts = new CancellationTokenSource ();
			dispatchLoopRuning = true;
			dispatchThreadObject = threadManager.StartThread (() => this.dispatchLoop ());
		}

		public void StopDispatchLoop ()
		{
			cts.Cancel ();
			mEvent.Set ();
		}

		public void RegisterActionHandler<T> (IStore storeObj, Action<T> handlerFn) where T:IAction
		{
			RegisterActionHandler<T> (storeObj, new IStore[]{ }, handlerFn);
		}

		public void RegisterActionHandler<T> (IStore storeObj, IStore[] dependencies, Action<T> handlerFn) where T:IAction
		{
			var newHandler = new ActionHandler {
				ActionType = typeof(T),
				HandlerStore = storeObj,
				Dependencies = new List<IStore> (dependencies),
				Priority = 1
			};
			newHandler.SetHandlerFn (handlerFn);
			lock (_actionHandlersLock) {
				var allHandlers = actionHandlersMap.ContainsKey (newHandler.ActionType) ? actionHandlersMap [newHandler.ActionType] : new List<ActionHandler> ();
				foreach (var handler in allHandlers) {
					if (handler.HandlerStore == newHandler.HandlerStore) {
						throw new RedundantActionHandlerException ("This store already have registered to handle this action");
					}
				}
				resolveDependencies (allHandlers, newHandler);
				allHandlers.Add (newHandler);
				allHandlers.Sort ((x, y) => x.Priority.CompareTo (y.Priority));
				if (!actionHandlersMap.ContainsKey (newHandler.ActionType))
					actionHandlersMap.Add (newHandler.ActionType, allHandlers);
				else
					actionHandlersMap [newHandler.ActionType] = allHandlers;
			}
		}

		private void resolveDependencies (List<ActionHandler> handlers, ActionHandler handler)
		{
			foreach (var dependency in handler.Dependencies) {
				var depHandler = handlers.Find (h => h.HandlerStore == dependency);
				if (depHandler == null) {
					throw new DependencyNotFound ("Dependency not found");
				}
				if (depHandler.Priority < handler.Priority) {
					depHandler.Priority = handler.Priority + 1;
					resolveDependencies (handlers, depHandler);
				}
			}
		}

		public void RegisterEventListener<T> (IStore storeObj, Action<T> listener) where T:IEvent
		{
			var eventType = typeof(T);
			Action<IEvent> wrappedListener = (IEvent e) => listener.Invoke ((T)e);
			lock (_eventHandlersLock) {
				if (eventHandlers.ContainsKey (storeObj)) {
					var eventsMap = eventHandlers [storeObj];
					if (eventsMap.ContainsKey (eventType)) {
						var eventsList = eventsMap [eventType];
						eventsList.Add (Tuple.Create<object, Action<IEvent>> (listener, wrappedListener));
					} else {
						eventsMap.Add (eventType, new List<Tuple<object, Action<IEvent>>> () { Tuple.Create<object, Action<IEvent>> (listener, wrappedListener) });
					}
				} else {
					eventHandlers.Add (storeObj, new Dictionary<Type, List<Tuple<object, Action<IEvent>>>> () { {
							eventType,
							new List<Tuple<object, Action<IEvent>>> () { Tuple.Create<object, Action<IEvent>> (listener, wrappedListener) }
						}
					});
				}
			}
		}

		public void UnregisterEventListener<T> (IStore storeObj, Action<T> listener) where T:IEvent
		{
			var eventType = typeof(T);
			lock (_eventHandlersLock) {
				if (eventHandlers.ContainsKey (storeObj)) {
					var eventsMap = eventHandlers [storeObj];
					if (eventsMap.ContainsKey (eventType)) {
						var handlersList = eventsMap [eventType];
						foreach (var handler in handlersList) {
							if (Object.ReferenceEquals (listener, handler.Item1)) {
								handlersList.Remove (handler);
								break;
							}
						}
					}

				}
			}
		}

		public void DispatchAction<T> (T action) where T:IAction
		{
			lock (_queueLock) {
				var actionType = typeof(T);
				if (!actionHandlersMap.ContainsKey (actionType))
					throw new NoHandlerForActionTypeException ("No handler for this action");
				actionsQueue.Enqueue (Tuple.Create<Type, IAction> (actionType, (IAction)action));
			}
			awakeDispatchLoop ();
		}

		public void DispatchEvent<T> (IStore storeObj, T eventObj) where T:IEvent
		{
			lock (_queueLock) {
				eventsQueue.Enqueue (Tuple.Create<IStore, Type, IEvent> (storeObj, typeof(T), (IEvent)eventObj));
			}
			awakeDispatchLoop ();
		}

		private void awakeDispatchLoop ()
		{
			mEvent.Set ();
		}
		private void dispatchLoop ()
		{
			while (dispatchLoopRuning) {
				mEvent.WaitHandle.WaitOne ();
				if (actionsQueue.Count > 0) {
					var action = actionsQueue.Dequeue ();
					var handlersToInvoke = new List<ActionHandler> ();
					lock (_actionHandlersLock) {
						var handlers = actionHandlersMap [action.Item1];
						foreach (var handler in handlers) {
							handlersToInvoke.Add (handler);
						}
					}
					foreach (var handler in handlersToInvoke) {
						handler.HandlerFn.Invoke (action.Item2);
					}
				}
				if (eventsQueue.Count > 0) {
					var eventTup = eventsQueue.Dequeue ();
					var handlersToInvoke = new List<Tuple<object, Action<IEvent>>> ();
					lock (_eventHandlersLock) {
						if (eventHandlers.ContainsKey (eventTup.Item1) && eventHandlers [eventTup.Item1].ContainsKey (eventTup.Item2)) {
							var handlers = eventHandlers [eventTup.Item1] [eventTup.Item2];
							foreach (var handler in handlers) {
								handlersToInvoke.Add (handler);
							}
						}
					}
					foreach (var handler in handlersToInvoke) {
						threadManager.RunOnMainThread (() => handler.Item2.Invoke (eventTup.Item3));
					}
				}
				lock (_queueLock) {
					if (actionsQueue.Count == 0 && eventsQueue.Count == 0) {
						mEvent.Reset ();
						dispatchLoopRuning &= !cts.Token.IsCancellationRequested;
					}
				}
			}
		}

		class ActionHandler
		{
			public Type ActionType{ get; set; }

			public IStore HandlerStore{ get; set; }

			public List<IStore> Dependencies{ get; set; }

			public Action<IAction> HandlerFn{ get; set; }

			public int Priority{ get; set; }

			public void SetHandlerFn<T> (Action<T> handler)
			{
				HandlerFn = (IAction action) => handler.Invoke ((T)action);
			}
		}
	}
}

