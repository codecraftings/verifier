using System;
using System.Threading.Tasks;

namespace Verifier.Core
{
	public interface IStore
	{
		void ListenForUpdate (Action<StoreUpdated> listenerFn);
	}

}

