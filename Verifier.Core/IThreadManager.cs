﻿using System;
using System.Threading;

namespace Verifier.Core
{
	public interface IThreadManager
	{
		object StartThread(Action runBlock);
		void RunOnMainThread(Action runBlock);
	}
}

