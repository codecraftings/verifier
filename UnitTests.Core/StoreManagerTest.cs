﻿using NUnit.Framework;
using System;
using Verifier.Core;
using Moq;
using System.Threading.Tasks;
using System.Threading;

namespace UnitTests.Core
{
	[TestFixture]
	public class StoreManagerTest
	{
		public class TestActionObject:IAction
		{
			public int PayLoad { get; set; }
		}

		public class AnotherTestActionObject:IAction
		{

		}

		public class TestEventObject:IEvent
		{

		}

		public class AnotherTestEventObject:IEvent
		{

		}

		[Test]
		public void A_Store_Can_Register_Handler_For_Specific_Action ()
		{
			var threadManagerMock = new Mock<IThreadManager> ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var mockStore = new Mock<IStore> ();
			var mockStore2 = new Mock<IStore> ();
			var mockStore3 = new Mock<IStore> ();
			storeManager.RegisterActionHandler<TestActionObject> (mockStore.Object, new IStore[]{ }, (TestActionObject obj) => {
			});
			storeManager.RegisterActionHandler<AnotherTestActionObject> (mockStore2.Object, new IStore[]{ }, (AnotherTestActionObject obj) => {
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore3.Object, new IStore[]{ }, (TestActionObject obj) => {
			});
		}

		[Test, ExpectedException (typeof(RedundantActionHandlerException))]
		public void A_Store_Can_Register_Only_Single_Handler_For_Specific_Action ()
		{
			var threadManagerMock = new Mock<IThreadManager> ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			var mockStore2 = new Mock<IStore> ();
			var mockStore3 = new Mock<IStore> ();
			storeManager.RegisterActionHandler<TestActionObject> (mockStore3.Object, new IStore[]{ }, (TestActionObject obj) => {
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore2.Object, new IStore[] {
				mockStore3.Object
			}, (TestActionObject obj) => {
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore3.Object, new IStore[]{ }, (TestActionObject obj) => {
			});
		}


		[Test, ExpectedException (typeof(DependencyNotFound))]
		public void When_Registering_Handlers_If_Dependency_Not_Found_It_Throws_Exception ()
		{
			var threadManagerMock = new Mock<IThreadManager> ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			var mockStore = new Mock<IStore> ();
			var mockStore2 = new Mock<IStore> ();
			var mockStore3 = new Mock<IStore> ();
			storeManager.RegisterActionHandler<TestActionObject> (mockStore3.Object, new IStore[]{ }, (TestActionObject obj) => {
			});
			storeManager.RegisterActionHandler<AnotherTestActionObject> (mockStore.Object, new IStore[]{ }, (AnotherTestActionObject obj) => {
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore2.Object, new IStore[] {
				mockStore.Object,
				mockStore3.Object
			}, (TestActionObject obj) => {
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore.Object, new IStore[]{ }, (TestActionObject obj) => {
			});
		}

		[Test]
		public void When_A_Action_Is_Dispatched_Registered_Handlers_Get_Executed ()
		{
			var threadManagerMock = getMockThreadManager ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var mockStore = new Mock<IStore> ();
			var mockStore2 = new Mock<IStore> ();
			var mockStore3 = new Mock<IStore> ();
			var someValue = 0;
			var someValue2 = 0;
			var someValue3 = 0;
			storeManager.RegisterActionHandler<TestActionObject> (mockStore.Object, new IStore[]{ }, (TestActionObject obj) => {
				someValue = 10;
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore2.Object, new IStore[]{ }, (TestActionObject obj) => {
				someValue2 = 10;
			});
			storeManager.RegisterActionHandler<AnotherTestActionObject> (mockStore3.Object, new IStore[]{ }, (AnotherTestActionObject obj) => {
				someValue3 = 10;
			});
			storeManager.DispatchAction<TestActionObject> (new TestActionObject ());
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();
			Assert.AreEqual (10, someValue);
			Assert.AreEqual (10, someValue2);
			Assert.AreEqual (0, someValue3);
		}

		[Test]
		public void When_Mulitple_Actions_Are_Dispatched_Registered_Handlers_Get_Executed ()
		{
			var threadManagerMock = getMockThreadManager ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var mockStore = new Mock<IStore> ();
			var mockStore2 = new Mock<IStore> ();
			var mockStore3 = new Mock<IStore> ();
			var someValue = 0;
			var someValue2 = 0;
			var someValue3 = 0;
			storeManager.RegisterActionHandler<TestActionObject> (mockStore.Object, new IStore[]{ }, (TestActionObject obj) => {
				someValue = 10;
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore2.Object, new IStore[]{ }, (TestActionObject obj) => {
				someValue2 = 10;
			});
			storeManager.RegisterActionHandler<AnotherTestActionObject> (mockStore3.Object, new IStore[]{ }, (AnotherTestActionObject obj) => {
				someValue3 = 10;
			});
			storeManager.DispatchAction<TestActionObject> (new TestActionObject ());
			storeManager.DispatchAction<AnotherTestActionObject> (new AnotherTestActionObject ());
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();
			Assert.AreEqual (10, someValue);
			Assert.AreEqual (10, someValue2);
			Assert.AreEqual (10, someValue3);
		}

		[Test]
		public void Action_Handlers_Gets_Executed_Syncronously ()
		{
			var threadManagerMock = getMockThreadManager ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var mockStore = new Mock<IStore> ();
			var mockStore2 = new Mock<IStore> ();
			var mockStore3 = new Mock<IStore> ();
			var someValue = 0;
			var someValue2 = 0;
			var someValue3 = 0;
			var i = 0;
			storeManager.RegisterActionHandler<TestActionObject> (mockStore.Object, new IStore[]{ }, (TestActionObject obj) => {
				someValue = i++;
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore2.Object, new IStore[]{ }, (TestActionObject obj) => {
				someValue2 = i++;
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore3.Object, new IStore[]{ }, (TestActionObject obj) => {
				someValue3 = i++;
			});
			storeManager.DispatchAction<TestActionObject> (new TestActionObject ());
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();
			Assert.AreEqual (0, someValue);
			Assert.AreEqual (1, someValue2);
			Assert.AreEqual (2, someValue3);
		}

		[Test]
		public void Dependencies_Should_Execute_First ()
		{
			var threadManagerMock = getMockThreadManager ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var mockStore = new Mock<IStore> ();
			var mockStore2 = new Mock<IStore> ();
			var mockStore3 = new Mock<IStore> ();
			var someValue = 0;
			storeManager.RegisterActionHandler<TestActionObject> (mockStore.Object, new IStore[]{ }, (TestActionObject obj) => {
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore3.Object, new IStore[]{ }, (TestActionObject obj) => {
				someValue = 100;
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore2.Object, new IStore[] {
				mockStore.Object,
				mockStore3.Object
			}, (TestActionObject obj) => {
				someValue = someValue == 100 ? 99 : 10;
			});
			storeManager.DispatchAction<TestActionObject> (new TestActionObject ());
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();
			Assert.AreEqual (99, someValue);
		}

		[Test]
		public void When_A_Action_Is_Dispatched_Action_Object_Is_Passed_To_Handler ()
		{
			var threadManagerMock = getMockThreadManager ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var mockStore = new Mock<IStore> ();
			var someValue = 0;
			var testActionObject = new TestActionObject () {
				PayLoad = 99
			};
			storeManager.RegisterActionHandler<TestActionObject> (mockStore.Object, new IStore[]{ }, (TestActionObject obj) => {
				Assert.AreSame (obj, testActionObject);
				someValue = obj.PayLoad;
			});
			storeManager.DispatchAction<TestActionObject> (testActionObject);
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();
			Assert.AreEqual (testActionObject.PayLoad, someValue);
		}

		[Test, ExpectedException (typeof(NoHandlerForActionTypeException))]
		public void When_A_Action_Is_Dispatch_But_No_Handler_Found_It_Throws_Exception ()
		{
			var threadManagerMock = new Mock<IThreadManager> ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			var mockStore = new Mock<IStore> ();
			var mockStore3 = new Mock<IStore> ();
			storeManager.RegisterActionHandler<TestActionObject> (mockStore3.Object, new IStore[]{ }, (TestActionObject obj) => {
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore.Object, new IStore[]{ }, (TestActionObject obj) => {
			});
			storeManager.DispatchAction<AnotherTestActionObject> (new AnotherTestActionObject ());
		}

		[Test]
		public void Actions_Can_Be_Dispatched_From_Different_Threads_But_Handlers_Are_Executed_On_Dispatch_Thread ()
		{
			var threadManagerMock = getMockThreadManager ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var mockStore = new Mock<IStore> ();
			var mockStore2 = new Mock<IStore> ();
			var mockStore3 = new Mock<IStore> ();
			int tID1 = 0, tID2 = 0, tID3 = 0, tID4 = 0, tID5 = 0;
			storeManager.RegisterActionHandler<TestActionObject> (mockStore.Object, new IStore[]{ }, (TestActionObject obj) => {
				tID1 = Thread.CurrentThread.ManagedThreadId;
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore3.Object, new IStore[]{ }, (TestActionObject obj) => {
				tID2 = Thread.CurrentThread.ManagedThreadId;
			});
			storeManager.RegisterActionHandler<AnotherTestActionObject> (mockStore2.Object, new IStore[] { }, (AnotherTestActionObject obj) => {
				tID3 = Thread.CurrentThread.ManagedThreadId;
			});
			var t1 = new Thread (delegate () {
				tID4 = Thread.CurrentThread.ManagedThreadId;
				storeManager.DispatchAction<TestActionObject> (new TestActionObject ());
			});
			var t2 = new Thread (delegate () {
				tID5 = Thread.CurrentThread.ManagedThreadId;
				storeManager.DispatchAction<AnotherTestActionObject> (new AnotherTestActionObject ());
			});
			t1.Start ();
			t2.Start ();
			t1.Join ();
			t2.Join ();
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();
			Assert.AreEqual (tID1, tID2);
			Assert.AreEqual (tID1, tID3);
			Assert.AreEqual (tID2, tID3);
			Assert.AreNotEqual (tID1, tID4);
			Assert.AreNotEqual (tID1, tID5);
		}

		[Test]
		public void Register_Event_Handler_For_Sepecific_Event_From_Specific_Store ()
		{
			var threadManagerMock = new Mock<IThreadManager> ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			var mockStore = new Mock<IStore> ();
			var mockStore2 = new Mock<IStore> ();
			var mockStore3 = new Mock<IStore> ();
			storeManager.RegisterEventListener<TestEventObject> (mockStore.Object, obj => {
			});
			storeManager.RegisterEventListener<TestEventObject> (mockStore.Object, obj => {
			});
			storeManager.RegisterEventListener<TestEventObject> (mockStore2.Object, obj => {
			});
			storeManager.RegisterEventListener<AnotherTestEventObject> (mockStore3.Object, obj => {
			});
		}

		[Test]
		public void When_A_Store_Event_Gets_Dispatched_Respective_Handlers_Gets_Executed ()
		{
			var threadManagerMock = getMockThreadManager ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var mockStore = new Mock<IStore> ();
			var mockStore2 = new Mock<IStore> ();
			var mockStore3 = new Mock<IStore> ();
			var someValue = 0;
			storeManager.RegisterEventListener<TestEventObject> (mockStore.Object, obj => {
				someValue += 10;
			});
			storeManager.RegisterEventListener<TestEventObject> (mockStore.Object, obj => {
				someValue += 10;
			});
			storeManager.RegisterEventListener<TestEventObject> (mockStore2.Object, obj => {
				someValue = 40;
			});
			storeManager.RegisterEventListener<AnotherTestEventObject> (mockStore3.Object, obj => {
				someValue = 30;
			});

			storeManager.RegisterEventListener<AnotherTestEventObject> (mockStore.Object, obj => {
				someValue += 10;
			});

			storeManager.DispatchEvent (mockStore.Object, new TestEventObject ());
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();
			Assert.AreEqual (20, someValue);
		}

		[Test]
		public void When_A_Store_Event_Gets_Dispatched_But_No_Handler_Found_It_Should_Silently_Pass ()
		{
			var threadManagerMock = getMockThreadManager ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var mockStore = new Mock<IStore> ();
			var mockStore2 = new Mock<IStore> ();
			var mockStore3 = new Mock<IStore> ();
			var someValue = 0;
			storeManager.RegisterEventListener<TestEventObject> (mockStore2.Object, obj => {
				someValue = 40;
			});
			storeManager.RegisterEventListener<AnotherTestEventObject> (mockStore3.Object, obj => {
				someValue = 30;
			});

			storeManager.RegisterEventListener<AnotherTestEventObject> (mockStore.Object, obj => {
				someValue = 10;
			});

			storeManager.DispatchEvent (mockStore.Object, new TestEventObject ());
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();
			Assert.AreEqual (0, someValue);
		}

		[Test]
		public void Event_Handlers_Should_Executed_On_Main_Thread ()
		{
			var threadManagerMock = getMockThreadManager ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var mockStore = new Mock<IStore> ();
			Action<TestEventObject> handlerAction = (obj) => {
			};
			storeManager.RegisterEventListener<TestEventObject> (mockStore.Object, handlerAction);

			storeManager.DispatchEvent (mockStore.Object, new TestEventObject ());
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();
			threadManagerMock.Verify (t => t.RunOnMainThread (It.IsAny<Action> ()), Times.Once);
		}

		[Test]
		public void If_Event_Handler_Get_Garbage_Collected_It_Should_Gracefully_Handle_That ()
		{
			var threadManagerMock = getMockThreadManager ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var mockStore = new Mock<IStore> ();
			var someValue = 0;
			Action bloc = () => {
				Action<TestEventObject> handlerAction = (obj) => {
					someValue = 10;
				};
				storeManager.RegisterEventListener<TestEventObject> (mockStore.Object, handlerAction);
			};
			bloc.Invoke ();
			System.GC.Collect ();
			storeManager.DispatchEvent (mockStore.Object, new TestEventObject ());
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();
			Assert.AreEqual (10, someValue);
		}

		[Test]
		public void It_Should_Unregister_Event ()
		{
			var threadManagerMock = getMockThreadManager ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var mockStore = new Mock<IStore> ();
			var someValue = 0;
			Action<TestEventObject> handlerAction = (obj) => {
				someValue = 10;
			};
			storeManager.RegisterEventListener<TestEventObject> (mockStore.Object, handlerAction);
			storeManager.UnregisterEventListener<TestEventObject> (mockStore.Object, handlerAction);
			storeManager.DispatchEvent (mockStore.Object, new TestEventObject ());
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();
			Assert.AreEqual (0, someValue);
		}
		[Test]
		public void When_There_Is_No_Action_Or_Event_In_Queue_Thread_Should_Sleep(){
			var threadManagerMock = getMockThreadManager ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var mockStore = new Mock<IStore> ();
			var mockStore2 = new Mock<IStore> ();
			var mockStore3 = new Mock<IStore> ();
			storeManager.RegisterActionHandler<TestActionObject> (mockStore.Object, new IStore[]{ }, (TestActionObject obj) => {
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore2.Object, new IStore[]{ }, (TestActionObject obj) => {
			});
			storeManager.RegisterActionHandler<TestActionObject> (mockStore3.Object, new IStore[]{ }, (TestActionObject obj) => {
			});
			var dispatchThread = storeManager.GetDispatchThread () as Thread;
			storeManager.DispatchAction<TestActionObject> (new TestActionObject ());
			//Assert.AreEqual (ThreadState.Background, dispatchThread.ThreadState);
			Thread.Sleep (500);
			Assert.IsTrue (dispatchThread.ThreadState.HasFlag(ThreadState.WaitSleepJoin));
			storeManager.StopDispatchLoop ();
			dispatchThread.Join ();
		}
		class DummyStoreState:IStoreState{

		}
		class DummyStore:AStore<DummyStoreState>{
			#region implemented abstract members of AStore

			protected override DummyStoreState getInitialStateObject ()
			{
				return new DummyStoreState ();
			}

			#endregion

			public DummyStore(ICacheManager cache, IStoreManager stMan):base(cache, stMan){

			}
			public void DoSomeChanges(){
				BroadcastUpdate ();
			}
		}
		[Test]
		public void Binding_Store_To_View_Controller(){
			//setup
			var threadManagerMock = getMockThreadManager ();
			var storeManager = new StoreManager (threadManagerMock.Object);
			storeManager.StartDispatchLoop ();
			var viewController = new Mock<IViewController> ();
			var store = new DummyStore (new Mock<ICacheManager> ().Object, storeManager);
			var iocMock = new Mock<IIoCContainer> ();
			viewController.SetupGet (v => v.IocContainer).Returns (iocMock.Object);
			viewController.SetupGet (v => v.IsViewLoaded).Returns (true);
			viewController.SetupGet (v => v.IsViewVisible).Returns (true);
			iocMock.Setup (i => i.Get<DummyStore> ()).Returns (store);

			//act
			var bindedStore = storeManager.Bind<DummyStore> (viewController.Object);
			store.DoSomeChanges ();

			//teardown
			storeManager.StopDispatchLoop ();
			var dispatchThread = storeManager.GetDispatchThread () as Thread;
			dispatchThread.Join ();

			//assert
			Assert.AreSame(bindedStore, store);
			viewController.Verify(v=>v.RenderState(), Times.Exactly(2));
		}
		static Mock<IThreadManager> getMockThreadManager ()
		{
			var threadManagerMock = new Mock<IThreadManager> ();
			threadManagerMock.Setup (thread => thread.StartThread (It.IsAny<Action> ())).Returns<Action> (action => {
				var dispatchThread = new Thread (action.Invoke) {
					IsBackground = true
				};
				dispatchThread.Start ();
				return dispatchThread;
			});
			threadManagerMock.Setup (thread => thread.RunOnMainThread (It.IsAny<Action> ())).Callback<Action> (action => action.Invoke ());
			return threadManagerMock;
		}

	}
}

