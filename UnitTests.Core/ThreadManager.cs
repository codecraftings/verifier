﻿using System;
using Verifier.Core;
using System.Threading;


namespace UnitTests.Core
{
	public class ThreadManager:IThreadManager
	{
		public ThreadManager ()
		{
		}

		#region IThreadManager implementation

		public object StartThread (Action runBlock)
		{
			var thread = new Thread (runBlock.Invoke);
			thread.Start ();
			return thread;
		}

		public void RunOnMainThread (Action runBlock)
		{
			runBlock.Invoke();
		}

		#endregion
	}
}

