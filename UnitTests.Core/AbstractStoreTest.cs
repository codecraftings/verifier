﻿using System;
using NUnit.Framework;
using Verifier.Core;
using Moq;
using Newtonsoft.Json;
using System.Threading;


namespace UnitTests.Core
{
	[TestFixture()]
	public class AbstractStoreTest
	{
		class TestStoreState:IStoreState{
			public string SomeData{get;set;}
		}
		class TestStore:AStore<TestStoreState>{
			#region implemented abstract members of AStore

			protected override TestStoreState getInitialStateObject ()
			{
				return new TestStoreState ();
			}

			#endregion

			public TestStore(ICacheManager cacheManager, IStoreManager storeManager):base(cacheManager, storeManager){

			}

			public void DoSomeChanges(){
				State.SomeData = "changed";
				BroadcastUpdate ();
			}
		}
		public AbstractStoreTest(){
		}

		[Test]
		public void Store_State_Should_Be_Saved_To_Cache(){
			var cacheManagerMock = new Mock<ICacheManager> ();
			var storeManager = new StoreManager (new ThreadManager ());
			storeManager.StartDispatchLoop ();
			var store = new TestStore (cacheManagerMock.Object, storeManager);
			store.State.SomeData = "22";
			storeManager.DispatchAction<DehydrateStoreAction> (new DehydrateStoreAction (store.GetType ()));
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();
			cacheManagerMock.Verify (c => c.Put<string> (It.Is<string>(s => s==store.GetType().FullName), It.Is<string>(s => s==JsonConvert.SerializeObject(store.State))), Times.Once);
		}

		[Test, Ignore]
		public void Store_State_Should_Be_Restored_From_Cache(){
			var cacheManagerMock = new Mock<ICacheManager> ();
			var storeManager = new StoreManager (new ThreadManager ());
			storeManager.StartDispatchLoop ();
			var store = new TestStore (cacheManagerMock.Object, storeManager);
			store.State.SomeData = "22";
			cacheManagerMock.Setup (c => c.Get<string> (It.Is<string> (s => s == store.GetType ().FullName))).Returns (
				"{SomeData:\"hello world\"}");
			storeManager.DispatchAction<RehydrateStoreAction> (new RehydrateStoreAction (store.GetType ()));
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();

			Assert.AreEqual ("hello world", store.State.SomeData);
		}

		[Test]
		public void Views_Can_Listen_For_Store_Update(){
			var cacheManagerMock = new Mock<ICacheManager> ();
			var storeManager = new StoreManager (new ThreadManager ());
			storeManager.StartDispatchLoop ();
			var store = new TestStore (cacheManagerMock.Object, storeManager);
			store.ListenForUpdate (e => {
				Assert.AreEqual("changed", store.State.SomeData);
			});
			store.DoSomeChanges ();
			storeManager.StopDispatchLoop ();
			(storeManager.GetDispatchThread () as Thread).Join ();
		}
	}
}

